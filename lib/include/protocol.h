/*
 * protocol.h
 *
 *  Created on: 20 апр. 2015 г.
 *      Author: klan
 */

#ifndef INCLUDE_PROTOCOL_H_
#define INCLUDE_PROTOCOL_H_

#include "config.h"
#include "string.h"

using namespace ConfigName;

#define MSG_TYPE_UAV 				0x02
#define MSG_TYPE_STATE_REQ 			0x04
#define MSG_TYPE_ANGLES 			0x06
#define MSG_TYPE_SETTINGS 			0x08
#define MSG_TYPE_ANGULAR_SPEED 		0x0A
#define MSG_TYPE_PHOTO_VIDEO 		0x0C
#define MSG_TYPE_SENSOR_ZERO 		0x0E

#define MSG_TYPE_OUT_ANGLES 		0x03
#define MSG_TYPE_OUT_STATE 			0x01

#define MSG_DATA_POWER				0x01
#define MSG_DATA_PHOTO				0x02
#define MSG_DATA_ZOOM				0x03
#define MSG_DATA_EXPOSURE			0x04
#define MSG_DATA_FREQUENCY			0x05
#define MSG_DATA_STABILISATION		0x06
#define MSG_DATA_CONTROL_PWM		0x07
#define MSG_DATA_COLOR				0x08
#define MSG_DATA_CAMERA				0x09


namespace ProtocolName {

class Protocol {
	ConfigurationMUSV_TypeDef config;
	bool inverted;
	int FindNextHead(uint8_t* data, int startIndex, uint8_t size);
	bool CheckSum(uint8_t* data, uint8_t startIndex, uint8_t size);
	float ByteToFloat(uint8_t* data, uint8_t startIndex);
	void FloatToByte(float number, uint8_t* data, uint8_t startIndex);
public:
	Protocol(bool inverted = false);
	~Protocol();
	void SetConfig(ConfigurationMUSV_TypeDef* config);
	ConfigurationMUSV_TypeDef* GetConfig();
	void DecodingData(uint8_t* data, uint8_t size);
	void CodingOutputAngleData(ConfigurationMUSV_TypeDef* config, uint8_t* data, uint8_t* size);
	void CodingOutputStateData(ConfigurationMUSV_TypeDef* config, uint8_t* data, uint8_t* size);
};

}  // namespace ProtocolName



#endif /* INCLUDE_PROTOCOL_H_ */
