/*
 * config.h
 *
 *  Created on: 21 апр. 2015 г.
 *      Author: klan
 */

#ifndef INCLUDE_CONFIG_H_
#define INCLUDE_CONFIG_H_
#include "stm32f4xx.h"

namespace ConfigName {

// MsgData for MsgType = 6 (00000110b)
typedef struct
{
	float roll;
	float pitch;
	float yaw;
} InputAngles_TypeDef;

// MsgData for MsgType = 8 (00001000b)
typedef enum
{
	SettingPower_On = 1,
	SettingPower_Off = 2,
	SettingPower_Wait = 3
} SettingPower_TypeDef;

typedef enum
{
	PhotoMode_NoShot = 0,
	PhotoMode_Single = 3,
	PhotoMode_Burst = 5
} PhotoMode_TypeDef;

typedef struct
{
	PhotoMode_TypeDef mode;
	uint16_t delay;
} SettingPhotoMode_TypeDef;

typedef enum
{
	SettingCameraMode_TV = 1,
	SettingCameraMode_IR = 2,
	SettingCameraMode_Photo = 4
} SettingCameraMode_TypeDef;

typedef enum
{
	SettingVideoMode_VBS = 1,
	SettingVideoMode_YPbPr = 2,
	SettingVideoMode_BT656 = 4
} SettingVideoMode_TypeDef;


typedef struct
{
	SettingPower_TypeDef power;
	SettingPhotoMode_TypeDef photoMode;
	uint8_t zoom;
	uint16_t exposure;
	uint8_t frequencyPosition;
	bool stabilization;
	bool controlPWM;
	uint8_t colorMode;
	SettingCameraMode_TypeDef cameraMode;
	SettingVideoMode_TypeDef videoMode;
} Setting_TypeDef;


// MsgData for MsgType = 3 (00000011b)
typedef struct
{
	float roll;
	float pitch;
	float yaw;
} OutputAngles_TypeDef;

// MsgData for MsgType = 10 (00001010b)
typedef struct
{
	float roll;
	float pitch;
	float yaw;
} InputAngularVelocity_TypeDef;

typedef struct
{
	bool requestState;
	InputAngles_TypeDef inputAngles;
	Setting_TypeDef settings;
	OutputAngles_TypeDef outputAngles;
	InputAngularVelocity_TypeDef inputAngularVelocity;
} ConfigurationMUSV_TypeDef;


}  // namespace ConfigName



#endif /* INCLUDE_CONFIG_H_ */
