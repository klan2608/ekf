/*
 * protocol.cpp
 *
 *  Created on: 20 апр. 2015 г.
 *      Author: klan
 */
#include "../lib/include/protocol.h"

using namespace ProtocolName;

Protocol::Protocol(bool inverted)
{

	this->inverted = inverted;
	config.inputAngles.roll = 0.f;
	config.inputAngles.pitch = -90.f;
	config.inputAngles.yaw = 0.f;
	config.inputAngularVelocity.roll = 0.f;
	config.inputAngularVelocity.pitch = 0.f;
	config.inputAngularVelocity.yaw = 0.f;
	config.outputAngles.roll = 0.f;
	config.outputAngles.pitch = -90.f;
	config.outputAngles.yaw = 0.f;
	config.requestState = false;
	config.settings.cameraMode = SettingCameraMode_TV;
	config.settings.colorMode = 1;
	config.settings.controlPWM = false;
	config.settings.exposure = 0;
	config.settings.frequencyPosition = 0;
	config.settings.photoMode.mode = PhotoMode_NoShot;
	config.settings.power = SettingPower_On;
	config.settings.stabilization = false;
	config.settings.videoMode = SettingVideoMode_VBS;
	config.settings.zoom = 1;
}

Protocol::~Protocol()
{

}

void Protocol::SetConfig(ConfigurationMUSV_TypeDef* config)
{
	this->config.outputAngles.roll = config->outputAngles.roll;
	this->config.outputAngles.pitch = config->outputAngles.pitch;
	this->config.outputAngles.yaw = config->outputAngles.yaw;

	this->config.settings.cameraMode = config->settings.cameraMode;
	this->config.settings.colorMode = config->settings.colorMode;
	this->config.settings.controlPWM = config->settings.controlPWM;
	this->config.settings.exposure = config->settings.exposure;
	this->config.settings.frequencyPosition = config->settings.frequencyPosition;
	this->config.settings.photoMode = config->settings.photoMode;
	this->config.settings.power = config->settings.power;
	this->config.settings.stabilization = config->settings.stabilization;
	this->config.settings.videoMode = config->settings.videoMode;
	this->config.settings.zoom = config->settings.zoom;
}

ConfigurationMUSV_TypeDef* Protocol::GetConfig()
{
	return &this->config;
}

float Protocol::ByteToFloat(uint8_t* data, uint8_t startIndex)
{
	float temp;
	uint8_t arr[4];
	arr[0] = data[startIndex + 3];
	arr[1] = data[startIndex + 2];
	arr[2] = data[startIndex + 1];
	arr[3] = data[startIndex];
	memcpy(&temp, arr, sizeof(temp));
	return temp;
}

void Protocol::FloatToByte(float number, uint8_t* data, uint8_t startIndex)
{
	uint8_t* p;
	p =  (uint8_t*) &number;
	memcpy(data+startIndex, p, 4);
	uint8_t tmp;
	tmp = data[startIndex];
	data[startIndex] = data[startIndex + 3];
	data[startIndex + 3] = tmp;
	tmp = data[startIndex + 1];
	data[startIndex + 1] = data[startIndex + 2];
	data[startIndex + 2] = tmp;
}

int Protocol::FindNextHead(uint8_t* data, int startIndex, uint8_t size)
{
	int i = startIndex;
	while(i < size)
	{
		switch(data[i])
		{
		case MSG_TYPE_UAV:
		case MSG_TYPE_STATE_REQ:
		case MSG_TYPE_ANGLES:
		case MSG_TYPE_OUT_ANGLES:
		case MSG_TYPE_SETTINGS:
		case MSG_TYPE_ANGULAR_SPEED:
		case MSG_TYPE_PHOTO_VIDEO:
		case MSG_TYPE_SENSOR_ZERO:
			return i;
			break;
		default:
			i++;
			break;
		}
	}
	return -1;
}

bool Protocol::CheckSum(uint8_t* data, uint8_t index, uint8_t size)
{
	uint8_t checkSum = 0;
	uint16_t i;
	for(i = index; i < (index+data[index+1]-1); i++)
	{
		if(i >= size)
			break;
		checkSum += data[i];
	}
	if(checkSum == data[i])
	{
		return true;
	}
	return false;
}

void Protocol::DecodingData(uint8_t* data, uint8_t size)
{
	int newIndex = -1;
	int lastIndex;
	do
	{
		lastIndex = newIndex;
		newIndex = FindNextHead(data, lastIndex+1, size);
		if(newIndex >= 0)
		{
			if(CheckSum(data, newIndex, size) == true)
			{
				switch(data[newIndex])
				{
				case MSG_TYPE_UAV:
					break;
				case MSG_TYPE_STATE_REQ:
					config.requestState = true;
					break;
				case MSG_TYPE_ANGLES:
				case MSG_TYPE_OUT_ANGLES:
					config.inputAngles.roll = ByteToFloat(data, newIndex+2);
					config.inputAngles.pitch = ByteToFloat(data, newIndex+6);
					config.inputAngles.yaw = ByteToFloat(data, newIndex+10);

					break;
				case MSG_TYPE_SETTINGS:
					switch(data[newIndex+2])
					{
					case MSG_DATA_POWER:
						config.settings.power = (SettingPower_TypeDef) data[newIndex+3];
						break;
					case MSG_DATA_PHOTO:
						config.settings.photoMode.mode = (PhotoMode_TypeDef) data[newIndex+3];
						config.settings.photoMode.delay = (uint16_t) ((data[newIndex+4] << 8) & data[newIndex+8]);
						break;
					case MSG_DATA_ZOOM:
						config.settings.zoom = data[newIndex+3];
						break;
					case MSG_DATA_EXPOSURE:
						config.settings.exposure = (uint16_t) ((data[newIndex+3] << 8) & data[newIndex+4]);
						break;
					case MSG_DATA_FREQUENCY:
						config.settings.frequencyPosition = data[newIndex+3];
						break;
					case MSG_DATA_STABILISATION:
						config.settings.stabilization = data[newIndex+3];
						break;
					case MSG_DATA_CONTROL_PWM:
						config.settings.controlPWM = data[newIndex+3];
						break;
					case MSG_DATA_COLOR:
						config.settings.colorMode = data[newIndex+3];
						break;
					case MSG_DATA_CAMERA:
						config.settings.cameraMode = (SettingCameraMode_TypeDef) data[newIndex+3];
						break;
					default:
						break;
					}
					break;
				case MSG_TYPE_ANGULAR_SPEED:
					config.inputAngularVelocity.roll = ByteToFloat(data, newIndex+2);
					config.inputAngularVelocity.pitch = ByteToFloat(data, newIndex+6);
					config.inputAngularVelocity.yaw = ByteToFloat(data, newIndex+10);
					break;
				case MSG_TYPE_PHOTO_VIDEO:
					break;
				case MSG_TYPE_SENSOR_ZERO:
					break;
				default:
					break;
				}
				data[newIndex] = 0;
			}
		}
	} while(newIndex >= 0);
}

void Protocol::CodingOutputAngleData(ConfigurationMUSV_TypeDef* config, uint8_t* data, uint8_t* size)
{
	if(inverted)
		data[0] = MSG_TYPE_ANGLES;
	else
		data[0] = MSG_TYPE_OUT_ANGLES;

	data[1] = 15;
	FloatToByte(config->outputAngles.roll, data, 2);
	FloatToByte(config->outputAngles.pitch, data, 6);
	FloatToByte(config->outputAngles.yaw, data, 10);
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 14; i++)
		sum += data[i];
	data[14] = sum;
	*size = 15;
}

void CodingOutputStateData(ConfigurationMUSV_TypeDef* config, uint8_t* data, uint8_t* size)
{
	data[0] = MSG_TYPE_OUT_STATE;
	data[1] = 12;
	data[2] = config->settings.cameraMode & 0x07;				// 0-2 bit 0 byte
	data[2] |= (config->settings.power & 0x03) << 3;			// 3-4 bit 0 byte
	data[2] |= (config->settings.videoMode & 0x07) << 5;		// 5-7 bit 0 byte
	data[3] = config->settings.photoMode.mode & 0x03;			// 0-1 bit 1 byte
	data[3] |= (config->settings.stabilization & 0x01) << 2;	// 2   bit 1 byte
	data[3] |= (config->settings.controlPWM & 0x01) << 3;		// 3   bit 1 byte
	data[4] = config->settings.photoMode.delay;
	data[5] = config->settings.photoMode.delay >> 8;
	data[6] = config->settings.zoom;
	data[7] = config->settings.exposure;
	data[8] = config->settings.exposure >> 8;
	data[9] = config->settings.frequencyPosition;
	data[10] = config->settings.colorMode;
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 11; i++)
		sum += data[i];
	data[11] = sum;
	*size = 12;
}
