/*
 * usart.cpp
 *
 *  Created on: 19 апр. 2015 г.
 *      Author: klan
 */
#include "../lib/include/usart.h"

using namespace UsartName;

Usart::Usart(Usart_TypeDef usartName, uint32_t baudRate)
{
	state = UsartState_Init;
	this->usartName = usartName;
	dataSendingComplete = true;
	memset(BufferForSend, 0, sizeof(BufferForSend));
	memset(BufferForRead, 0, sizeof(BufferForRead));
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = baudRate;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;

	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_4; 				// Only USART6 = DMA_Channel_5
//	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
//	DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForSend);
//	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &BufferForSend;
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART2->DR);
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	switch(usartName)
	{
	case Usart_1:		// TX - PA9, RX - PA10
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
		NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream7_IRQn;
		port = GPIOA;
		usart = USART1;
		dmaStreamTx = DMA2_Stream7;
		dmaStreamRx = DMA2_Stream5;	//2
		GPIO_PinAFConfig(port, GPIO_PinSource9, GPIO_AF_USART1);
		GPIO_PinAFConfig(port, GPIO_PinSource10, GPIO_AF_USART1);
		break;
	case Usart_1_Remap:		// TX - PB6, RX - PB7
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
		NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream7_IRQn;
		port = GPIOB;
		usart = USART1;
		dmaStreamTx = DMA2_Stream7;
		dmaStreamRx = DMA2_Stream2;
		GPIO_PinAFConfig(port, GPIO_PinSource6, GPIO_AF_USART1);
		GPIO_PinAFConfig(port, GPIO_PinSource7, GPIO_AF_USART1);
		break;
	case Usart_2:		// TX - PA2, RX - PA3
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream6_IRQn;
		port = GPIOA;
		usart = USART2;
		dmaStreamTx = DMA1_Stream6;
		dmaStreamRx = DMA1_Stream5;
		GPIO_PinAFConfig(port, GPIO_PinSource2, GPIO_AF_USART2);
		GPIO_PinAFConfig(port, GPIO_PinSource3, GPIO_AF_USART2);
		break;
	case Usart_3:		// TX - PB10, RX - PB11
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream3_IRQn;
		port = GPIOB;
		usart = USART3;
		dmaStreamTx = DMA1_Stream3;
		dmaStreamRx = DMA1_Stream1;
		GPIO_PinAFConfig(port, GPIO_PinSource10, GPIO_AF_USART3);
		GPIO_PinAFConfig(port, GPIO_PinSource11, GPIO_AF_USART3);
		break;
	case Usart_3_Remap:		// TX - PC10, RX - PC11
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream3_IRQn;
		port = GPIOC;
		usart = USART3;
		dmaStreamTx = DMA1_Stream3;
		dmaStreamRx = DMA1_Stream1;
		GPIO_PinAFConfig(port, GPIO_PinSource10, GPIO_AF_USART3);
		GPIO_PinAFConfig(port, GPIO_PinSource11, GPIO_AF_USART3);
		break;
	case Usart_4:		// TX - PA0, RX - PA1
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream4_IRQn;
		port = GPIOA;
		usart = UART4;
		dmaStreamTx = DMA1_Stream4;
		dmaStreamRx = DMA1_Stream2;
		GPIO_PinAFConfig(port, GPIO_PinSource0, GPIO_AF_UART4);
		GPIO_PinAFConfig(port, GPIO_PinSource1, GPIO_AF_UART4);
		break;
	case Usart_4_Remap:		// TX - PC10, RX - PC11
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream4_IRQn;
		port = GPIOC;
		usart = UART4;
		dmaStreamTx = DMA1_Stream4;
		dmaStreamRx = DMA1_Stream2;
		GPIO_PinAFConfig(port, GPIO_PinSource10, GPIO_AF_UART4);
		GPIO_PinAFConfig(port, GPIO_PinSource11, GPIO_AF_UART4);
		break;
	case Usart_6:		// TX - PC6, RX - PC7
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
		NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream6_IRQn;
		port = GPIOC;
		usart = USART6;
		dmaStreamTx = DMA2_Stream6;
		dmaStreamRx = DMA2_Stream1;
		GPIO_PinAFConfig(port, GPIO_PinSource6, GPIO_AF_USART6);
		GPIO_PinAFConfig(port, GPIO_PinSource7, GPIO_AF_USART6);
		break;
	default:
		state = UsartState_Error;
	}

	if(state == UsartState_Init)
	{
		GPIO_Init(port, &GPIO_InitStructure);
		USART_Init(usart, &USART_InitStructure);
		//DMA TX Init
		DMA_DeInit(dmaStreamTx);
		DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForSend);
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &BufferForSend;
		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(usart->DR);
		DMA_Init(dmaStreamTx, &DMA_InitStructure);
		DMA_Cmd(dmaStreamTx, DISABLE);
		USART_DMACmd(usart, USART_DMAReq_Tx, ENABLE);
		DMA_ITConfig(dmaStreamTx, DMA_IT_TC, ENABLE);
		NVIC_Init(&NVIC_InitStructure);
		//DMA RX Init
		DMA_DeInit(dmaStreamRx);
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
		DMA_InitStructure.DMA_BufferSize = 127;
		//DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForRead);
		DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &BufferForRead;
		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(usart->DR);
		DMA_Init(dmaStreamRx, &DMA_InitStructure);
		DMA_Cmd(dmaStreamRx, ENABLE);
		USART_DMACmd(usart, USART_DMAReq_Rx, ENABLE);

		USART_Cmd(usart, ENABLE);
		state = UsartState_On;
	}

}

Usart::~Usart()
{

}

void Usart::Switch(UsartState_TypeDef state)
{
	if(state == UsartState_On)
	{
		DMA_Cmd(dmaStreamTx, ENABLE);
		DMA_Cmd(dmaStreamRx, ENABLE);
		USART_Cmd(usart, ENABLE);
	}
	else if(state == UsartState_Off)
	{
		DMA_Cmd(dmaStreamTx, DISABLE);
		DMA_Cmd(dmaStreamRx, DISABLE);
		USART_Cmd(usart, DISABLE);
	}
}

void Usart::SendData(uint8_t* data, uint8_t size)
{
	if((size <= sizeof(BufferForSend)) && (dataSendingComplete == true))
	{
		memset(BufferForSend, 0, sizeof(BufferForSend));
		memcpy(BufferForSend, data, size);
		dataSendingComplete = false;
		DMA_SetCurrDataCounter(dmaStreamTx, size);
		DMA_Cmd(dmaStreamTx, ENABLE);
	}
}

void Usart::SendToTerminalNMEA(MPU6000Data* mpu6000Data, uint32_t time, uint8_t modeSender)
{
	memset(BufferForSend, 0, sizeof(BufferForSend));

	switch (modeSender)
	{
	//only gyro
	case 1:
		sprintf(BufferForSend, "$GBGYR,%f,%f,%f*00\n", mpu6000Data->Gyro.X,
				mpu6000Data->Gyro.Y, mpu6000Data->Gyro.Z);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
		//only accel
	case 2:
		sprintf(BufferForSend, "$GBACC,%f,%f,%f*00\n", mpu6000Data->Accel.X,
				mpu6000Data->Accel.Y, mpu6000Data->Accel.Z);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
		//only MAG
	case 3:
		sprintf(BufferForSend, "$GBMAG,%f,%f,%f*00\n", mpu6000Data->Mag.X,
				mpu6000Data->Mag.Y, mpu6000Data->Mag.Z);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
		//only angle
	case 4:
		sprintf(BufferForSend, "$GBANG,%f,%f,%f*00\n", mpu6000Data->Mag.X,
				mpu6000Data->roll, mpu6000Data->pitch, mpu6000Data->yaw);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
		//only Time
	case 5:
		sprintf(BufferForSend, "$GBTMR,%d*00\n", time);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
	default:
		sprintf(BufferForSend,
				"$GBGYR,%f,%f,%f*00\n$GBACC,%f,%f,%f*00\n$GBMAG,%f,%f,%f*00\n$GBTMP,%f*00\n$GBTMR,%d*00\n$GBANG,%f,%f,%f*00\n",
				mpu6000Data->Gyro.X, mpu6000Data->Gyro.Y, mpu6000Data->Gyro.Z,
				mpu6000Data->Accel.X, mpu6000Data->Accel.Y, mpu6000Data->Accel.Z,
				mpu6000Data->Mag.X, mpu6000Data->Mag.Y, mpu6000Data->Mag.Z,
				mpu6000Data->Temp, time,
				mpu6000Data->roll, mpu6000Data->pitch, mpu6000Data->yaw);
		DMA_Cmd(dmaStreamTx, ENABLE);
		break;
	}

//	if(mpu6000Data.Mag.X > 2.f || mpu6000Data.Mag.X < -2.f ||
//			mpu6000Data.Mag.Y > 2.f || mpu6000Data.Mag.Y < -2.f ||
//			mpu6000Data.Mag.Z > 2.f || mpu6000Data.Mag.Z < -2.f)
//	{
//		GPIO_ToggleBits(GPIOC, GPIO_Pin_10);
//	}
}

uint8_t* Usart::GetReadData(void)
{
	return BufferForRead;
}

uint16_t Usart::GetReadDataSize(void)
{
	return (uint16_t)(sizeof(BufferForRead) - DMA_GetCurrDataCounter(dmaStreamRx));
}

