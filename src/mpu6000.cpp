/*
 * mpu6000.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#include "inc/mpu6000.h"


extern volatile bool gyroInit;
NMEAParser nmeaParse;

void MPUInit()
{
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_PWR_MGMT_1, BIT_H_RESET);          // Device Reset
	Delay(250000);
	SPI_WriteData(SPI1, MPUREG_PWR_MGMT_1, BIT_CYCLE);          // Device Reset
	Delay(250000);
	SPI_WriteData(SPI1, MPUREG_PWR_MGMT_1, MPU_CLK_SEL_PLLGYROX);          // Clock Source PPL with Z axis gyro reference
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_USER_CTRL, BIT_I2C_IF_DIS);           // Disable Primary I2C Interface
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_PWR_MGMT_2, 0x00);
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_SMPLRT_DIV, SAMPLE_RATE_25Hz);					// Accel Sample Rate 1000 Hz, Gyro Sample Rate 8000 Hz
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_CONFIG, BIT_DLPF_CFG_10Hz);              // Accel and Gyro DLPF Setting
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_ACCEL_CONFIG, BIT_AFS_SEL_4G);        // Accel +/- 4 G Full Scale
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_GYRO_CONFIG, BITS_FS_250DPS);         // Gyro +/- 1000 DPS Full Scale
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_INT_ENABLE, BIT_RAW_RDY_EN);
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_INT_PIN_CFG, BIT_INT_ANYRD_2CLEAR);
	Delay(100);

	HMC5883_Init();

	MPUInteraptInit();

//	Calibration without MUSV
//	nmeaParse.calibData.biasX = -0.000917494f;
//	nmeaParse.calibData.biasY = 0.113761f;
//	nmeaParse.calibData.biasZ = -0.025688f;
//	nmeaParse.calibData.scaleX = 2.40088f;
//	nmeaParse.calibData.scaleY = 2.43303f;
//	nmeaParse.calibData.scaleZ = 2.51152f;

	//	Calibration with MUSV
	nmeaParse.calibData.biasX = 0.279358f;
	nmeaParse.calibData.biasY = 0.136698f;
	nmeaParse.calibData.biasZ = -0.0977065f;
	nmeaParse.calibData.scaleX = 2.28751f;
	nmeaParse.calibData.scaleY = 2.43848f;
	nmeaParse.calibData.scaleZ = 2.6683f;
}

void HMC5883_Init()
{
	SPI_WriteData(SPI1, MPUREG_INT_PIN_CFG, 0x12);	//i2c bypass on
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_USER_CTRL, 0x00);	//i2c master mode disable
	Delay(100);
	SPI_WriteData(SPI1, MPUREG_INT_PIN_CFG, BIT_INT_ANYRD_2CLEAR);	//i2c bypass off
	Delay(100);
	SPI_WriteData(SPI1, I2C_MST_CTRL, BIT_I2C_MST_P_NSR | BIT_I2C_MST_CLK_400kHz);	//P_NSR, 400kHz
	Delay(100);
	//read MAG
	SPI_WriteData(SPI1, I2C_SLV0_ADDR, I2C_SLV0_R | HMC5883_ADDR);	//READ
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV0_REG, 0x09);	//reg (status)
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV0_CTRL, I2C_SLV0_EN | 13); //Slave0 enable 13 bytes
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV1_ADDR, HMC5883_ADDR);	//WRITE
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV1_REG, 0x02);  //reg (MODE)
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV1_CTRL, I2C_SLV1_EN | 1); //Slave1 enable 1 byte
	Delay(100);
	SPI_WriteData(SPI1, I2C_SLV1_DO, 1); //reg value (single measurement)
	Delay(100);
	//delays, MASTER enable
	SPI_WriteData(SPI1, I2C_SLV4_CTRL, 0); //MST_DLY = 25/1 = 25 [Hz]
	Delay(100);
	SPI_WriteData(SPI1, I2C_MST_DELAY_CTRL, BIT_DELAY_ES_SHADOW | BIT_I2C_SLV3_DLY_EN | BIT_I2C_SLV1_DLY_EN); //delay slaves
	Delay(100);
	SPI_WriteData(SPI1, USER_CTRL, BIT_I2C_MST_EN); //MASTER enable
	Delay(100);
	//interrupts
	SPI_WriteData(SPI1, INT_PIN_CFG, BIT_INT_RD_CLEAR | BIT_INT_LATCN_INT_EN); //interrupt LATCH_INT_EN
	Delay(100);
	SPI_WriteData(SPI1, INT_ENABLE, BIT_DATA_RDY_EN); //interrupt DATA_DRY_EN
	Delay(100);
}

void MPUReadData(MPU6000Data* mpu6000Data)
{
	Delay(50);
	mpu6000Data->Accel.X_HL = SPI_ReadData(SPI1, MPUREG_ACCEL_XOUT_H)<<8;
	Delay(2);
	mpu6000Data->Accel.X_HL |= SPI_ReadData(SPI1, MPUREG_ACCEL_XOUT_L);
	mpu6000Data->Accel.X = AccelKoef * mpu6000Data->Accel.X_HL;
	Delay(2);
	mpu6000Data->Accel.Y_HL = SPI_ReadData(SPI1, MPUREG_ACCEL_YOUT_H)<<8;
	Delay(2);
	mpu6000Data->Accel.Y_HL |= SPI_ReadData(SPI1, MPUREG_ACCEL_YOUT_L);
	mpu6000Data->Accel.Y = -AccelKoef * mpu6000Data->Accel.Y_HL;
	Delay(2);
	mpu6000Data->Accel.Z_HL = SPI_ReadData(SPI1, MPUREG_ACCEL_ZOUT_H)<<8;
	Delay(2);
	mpu6000Data->Accel.Z_HL |= SPI_ReadData(SPI1, MPUREG_ACCEL_ZOUT_L);
	mpu6000Data->Accel.Z = -AccelKoef * mpu6000Data->Accel.Z_HL;
	Delay(2);
	mpu6000Data->Gyro.X_HL = SPI_ReadData(SPI1, MPUREG_GYRO_XOUT_H)<<8;
	Delay(2);
	mpu6000Data->Gyro.X_HL |= SPI_ReadData(SPI1, MPUREG_GYRO_XOUT_L);
	mpu6000Data->Gyro.X = GyroKoef * mpu6000Data->Gyro.X_HL * Deg2Rad;
	Delay(2);
	mpu6000Data->Gyro.Y_HL = SPI_ReadData(SPI1, MPUREG_GYRO_YOUT_H)<<8;
	Delay(2);
	mpu6000Data->Gyro.Y_HL |= SPI_ReadData(SPI1, MPUREG_GYRO_YOUT_L);
	mpu6000Data->Gyro.Y = -GyroKoef * mpu6000Data->Gyro.Y_HL * Deg2Rad;
	Delay(2);
	mpu6000Data->Gyro.Z_HL = SPI_ReadData(SPI1, MPUREG_GYRO_ZOUT_H)<<8;
	Delay(2);
	mpu6000Data->Gyro.Z_HL |= SPI_ReadData(SPI1, MPUREG_GYRO_ZOUT_L);
	mpu6000Data->Gyro.Z = -GyroKoef * mpu6000Data->Gyro.Z_HL * Deg2Rad;
	Delay(2);

	mpu6000Data->Temp_HL = SPI_ReadData(SPI1, MPUREG_TEMP_OUT_H)<<8;
	Delay(2);
	mpu6000Data->Temp_HL |= SPI_ReadData(SPI1, MPUREG_TEMP_OUT_L);
	mpu6000Data->Temp = 1.0f / 340.f * (mpu6000Data->Temp_HL) + 36.53f;
	mpu6000Data->DataReady = 0x00;

	HMCReadData(mpu6000Data);
}

void HMCReadData(MPU6000Data* mpu6000Data)
{
	mpu6000Data->DataReadyMag = SPI_ReadData(SPI1, EXT_SENS_DATA_00) & 0x01;
	Delay(2);

	mpu6000Data->Mag.X_HL = SPI_ReadData(SPI1, EXT_SENS_DATA_07);
	mpu6000Data->Mag.X_HL = mpu6000Data->Mag.X_HL<<8;
	Delay(2);
	mpu6000Data->Mag.X_HL |= SPI_ReadData(SPI1, EXT_SENS_DATA_08);
	mpu6000Data->Mag.X = (float) (MagKoef * ((short)(mpu6000Data->Mag.X_HL)) - nmeaParse.calibData.biasX) * nmeaParse.calibData.scaleX;
	Delay(2);
	mpu6000Data->Mag.Z_HL = SPI_ReadData(SPI1, EXT_SENS_DATA_09);
	mpu6000Data->Mag.Z_HL = mpu6000Data->Mag.Z_HL<<8;
	Delay(2);
	mpu6000Data->Mag.Z_HL |= SPI_ReadData(SPI1, EXT_SENS_DATA_10);
	mpu6000Data->Mag.Z = (float) (-MagKoef * ((short)(mpu6000Data->Mag.Z_HL)) - nmeaParse.calibData.biasZ) * nmeaParse.calibData.scaleZ;
	Delay(2);
	mpu6000Data->Mag.Y_HL = SPI_ReadData(SPI1, EXT_SENS_DATA_11);
	mpu6000Data->Mag.Y_HL = mpu6000Data->Mag.Y_HL<<8;
	Delay(2);
	mpu6000Data->Mag.Y_HL |= SPI_ReadData(SPI1, EXT_SENS_DATA_12);
	mpu6000Data->Mag.Y = (float) (-MagKoef * ((short)(mpu6000Data->Mag.Y_HL)) - nmeaParse.calibData.biasY) * nmeaParse.calibData.scaleY;
	Delay(2);
}


uint8_t MPUReadID()
{
	return SPI_ReadData(SPI1, MPUREG_WHOAMI);
}


void MPUInteraptInit()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource11);
	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line11;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
