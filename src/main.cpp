#include "inc/led.h"
#include "inc/spi.h"
//#include "inc/usart.h"
#include "inc/mpu6000.h"
#include "inc/delay.h"
#include "inc/filter.h"
#include "../NMEAParser/NMEAParser.h"

#include "../lib/include/usart.h"
#include "../lib/include/protocol.h"
#include "../lib/include/config.h"

using namespace UsartName;
using namespace ConfigName;
using namespace ProtocolName;


uint8_t cycleIter;
RCC_ClocksTypeDef RCC_Clocks;
volatile uint8_t tempAnswerSPI;
volatile uint8_t ID;
volatile uint32_t timeNow, timeLast;
volatile uint32_t time;
volatile float freq;
volatile bool gyroInit;

MPU6000Data mpu6000Data;

Usart* uartUAV;
Usart* uartMUSV;

Protocol* protocolUAV;
Protocol* protocolMUSV;

ConfigurationMUSV_TypeDef* configUAV;
ConfigurationMUSV_TypeDef* configMUSV;


void MUSV_Update()
{
	protocolUAV->DecodingData(uartUAV->GetReadData(), uartUAV->GetReadDataSize());
	protocolMUSV->DecodingData(uartMUSV->GetReadData(), uartMUSV->GetReadDataSize());

	configUAV->outputAngles.roll = -1*mpu6000Data.roll;
	configUAV->outputAngles.pitch = mpu6000Data.pitch;
	configUAV->outputAngles.yaw = mpu6000Data.yaw;

//	MUSV.out"set" = MUSV.in"encoder" + (UAV.in"finish" - UAV.out"current")
 	configMUSV->outputAngles.roll = configMUSV->inputAngles.roll + (configUAV->inputAngles.roll - configUAV->outputAngles.roll);
	configMUSV->outputAngles.pitch = configMUSV->inputAngles.pitch + (configUAV->inputAngles.pitch - (configUAV->outputAngles.pitch - 90.f));

	if((configMUSV->outputAngles.roll < -90.f) && (configMUSV->outputAngles.roll > -150.f))
		configMUSV->outputAngles.roll = -90.f;

//	configMUSV->outputAngles.roll = configMUSV->inputAngles.roll;
//	configMUSV->outputAngles.pitch = configMUSV->inputAngles.pitch;
//	configMUSV->outputAngles.yaw = configMUSV->inputAngles.yaw;
//
//	configUAV->outputAngles.roll = configUAV->inputAngles.roll;
//	configUAV->outputAngles.pitch = configUAV->inputAngles.pitch;
//	configUAV->outputAngles.yaw = configUAV->inputAngles.yaw;


	uint8_t bufMUSV[20];
	uint8_t sizeBufMUSV;
	uint8_t bufUAV[20];
	uint8_t sizeBufUAV;

	protocolMUSV->CodingOutputAngleData(configMUSV, bufMUSV, &sizeBufMUSV);
	protocolUAV->CodingOutputAngleData(configUAV, bufUAV, &sizeBufUAV);

	uartMUSV->SendData(bufMUSV, sizeBufMUSV);
//	uartUAV->SendData(bufUAV, sizeBufUAV);
	uartUAV->SendToTerminalNMEA(&mpu6000Data, time, 0);
}

void main()
{
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000000); //microsecond
	RCC_GetClocksFreq(&RCC_Clocks);

	LedInit();
//	USART2Init();
	Spi1Init();
	MPUInit();

	uartUAV = new Usart(Usart_1, 115200);
	protocolUAV = new Protocol();
	configUAV = protocolUAV->GetConfig();

	uartMUSV = new Usart(Usart_2, 115200);
	protocolMUSV = new Protocol(true);
	configMUSV = protocolMUSV->GetConfig();

	mpu6000Data.initX = 0;
	mpu6000Data.initY = 0;
	mpu6000Data.initZ = 0;
	uint8_t initIter = 0;
	gyroInit = true;
	while(initIter < 250)
	{
		if(mpu6000Data.DataReady == 0x01)
		{
			MPUReadData(&mpu6000Data);
			mpu6000Data.initX += mpu6000Data.Gyro.X;
			mpu6000Data.initY += mpu6000Data.Gyro.Y;
			mpu6000Data.initZ += mpu6000Data.Gyro.Z;
			initIter++;
		}
	}
	mpu6000Data.initX /= initIter;
	mpu6000Data.initY /= initIter;
	mpu6000Data.initZ /= initIter;

	gyroInit = false;

	InitFilter();

	while (1)
	{
		if (mpu6000Data.DataReady == 0x01)
		{
			mpu6000Data.DataReady = 0x00;
			timeNow = Timer_GetTick();
			time = timeNow - timeLast;
			freq = 1000.0f / time;
			timeLast = timeNow;

			GPIO_ToggleBits(LED_2_PORT, LED_2_PIN);

			MPUReadData(&mpu6000Data);

			UpdateFilter(0.040f);

			cycleIter++;

			if (cycleIter == 1)
			{
				GPIO_SetBits(LED_1_PORT, LED_1_PIN);
			}
			else if (cycleIter == 5)
			{
				GPIO_ResetBits(LED_1_PORT, LED_1_PIN);
				cycleIter = 0;
				MUSV_Update();
				uartUAV->SendToTerminalNMEA(&mpu6000Data, time, 0);
			}



//			modeSender = 0;
//			SendToTerminalNMEA(&mpu6000Data);
		}
	}
}


extern "C"
{
void SysTick_Handler(void)
{
	Timer_IncTick();
}

void EXTI15_10_IRQHandler(void)
{
	mpu6000Data.DataReady = 0x01;
	EXTI_ClearITPendingBit(EXTI_Line11);
}

void DMA2_Stream7_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA2_Stream7, DMA_IT_TCIF7))
	{
		uartUAV->dataSendingComplete = true;
		DMA_Cmd(DMA2_Stream7, DISABLE);
		DMA_ClearITPendingBit(DMA2_Stream7, DMA_IT_TCIF7);
	}
}

void DMA1_Stream6_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_Stream6, DMA_IT_TCIF6))
	{
		uartMUSV->dataSendingComplete = true;
		DMA_Cmd(DMA1_Stream6, DISABLE);
		DMA_ClearITPendingBit(DMA1_Stream6, DMA_IT_TCIF6);
	}
}

//void DMA1_Stream6_IRQHandler(void)
//{
//  if (DMA_GetITStatus(DMA1_Stream6, DMA_IT_TCIF6))
//  {
//	DMA_Cmd(DMA1_Stream6, DISABLE);
//    DMA_ClearITPendingBit(DMA1_Stream6, DMA_IT_TCIF6);
//  }
//}
//
//void USART2_IRQHandler(void)
//{
//	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
//	{
//		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
//	}
//}

}
