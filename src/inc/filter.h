/*
 * filter.h
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#ifndef INC_FILTER_H_
#define INC_FILTER_H_

#include "../EKF/ekfTypes.h"
#include <string.h>
#include "../EKF/perform_ekf.h"
#include "../EKF/consts.h"
#include "mpu6000.h"


void InitFilter(void);
void UpdateFilter ( FLOAT_TYPE secElapsed );

#endif /* INC_FILTER_H_ */
