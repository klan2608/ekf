/*
 * mpu6000.h
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#ifndef MPU6000_H_
#define MPU6000_H_

#include "spi.h"
#include "MPU6000.h"
#include "MPU60X0_RM.h"
#include "HMC8553L.h"
#include "../EKF/consts.h"
#include "delay.h"
#include "../NMEAParser/NMEAParser.h"


struct Sensor
{
	int16_t X_HL;
	float X;
	int16_t Y_HL;
	float Y;
	int16_t Z_HL;
	float Z;
};

struct MPU6000Data
{
	Sensor Accel;
	Sensor Gyro;
	Sensor GyroInit;
	Sensor Mag;
	uint16_t Temp_HL;
	float Temp;
	volatile uint8_t DataReady;
	volatile uint8_t DataReadyMag;
	float initX;
	float initY;
	float initZ;
	float roll;
	float pitch;
	float yaw;
};

extern NMEAParser nmeaParse;


static float AccelKoef = 9.81f / 8192.f;
static float GyroKoef = 1.0f / 131.0f;
static float MagKoef = 1.0f / 1090;


void MPUInit(void);
void HMC5883_Init(void);
void MPUReadData(MPU6000Data* mpu6000Data);
void HMCReadData(MPU6000Data* mpu6000Data);
uint8_t MPUReadID(void);
void MPUInteraptInit(void);

#endif /* MPU6000_H_ */
