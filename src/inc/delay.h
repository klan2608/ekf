/*
 * delay.h
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#ifndef INC_DELAY_H_
#define INC_DELAY_H_

#include "stm32f4xx_rcc.h"
#include <stdint.h>


void Timer_InitTick(uint32_t TickDiv, uint32_t TickPriority);
void Timer_IncTick(void);
uint32_t Timer_GetTick(void);
void Delay(uint32_t time);

#endif /* INC_DELAY_H_ */
