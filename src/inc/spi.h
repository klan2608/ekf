/*
 * spi.h
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#ifndef SPI_H_
#define SPI_H_

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_spi.h"
#include "delay.h"

#define SPI_TIME_OUT 	100
#define SPI_PORT		GPIOB
#define SPI_MISO_PIN	GPIO_Pin_5
#define SPI_MOSI_PIN	GPIO_Pin_4
#define SPI_CLK_PIN		GPIO_Pin_3
#define SPI_CS_PORT		GPIOA
#define SPI_CS_PIN		GPIO_Pin_4

extern volatile uint8_t tempAnswerSPI;

void Spi1Init(void);
void SPI_WriteData(SPI_TypeDef *spi, uint8_t addr, uint8_t data);
uint8_t SPI_ReadData(SPI_TypeDef *spi, uint8_t addr);
void SPI_CS_Cmd(FunctionalState state);

#endif /* SPI_H_ */
