/*
 * led.h
 *
 *  Created on: Mar 10, 2015
 *      Author: Y. Reznikov
 */

#ifndef LED_H_
#define LED_H_

#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

#define LED_1_PORT 		GPIOA
#define LED_1_PIN 		GPIO_Pin_0
#define LED_2_PORT 		GPIOA
#define LED_2_PIN 		GPIO_Pin_1
#define LED_3_PORT 		GPIOA
#define LED_3_PIN 		GPIO_Pin_15
#define LED_4_PORT 		GPIOC
#define LED_4_PIN 		GPIO_Pin_10

void LedInit(void);

#endif /* LED_H_ */
