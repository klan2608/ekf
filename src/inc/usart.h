/*
 * usart.h
 *
 *  Created on: Mar 10, 2015
 *      Author: yura
 */

#ifndef USART_H_
#define USART_H_

#include "stm32f4xx_usart.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_dma.h"
#include <stdio.h>
#include <string.h>
#include "mpu6000.h"

extern volatile uint32_t time;

extern volatile uint8_t modeSender;

void USART2Init(void);
void SendToTerminal(MPU6000Data* mpu6000Data);
void UsartDmaInit(void);
void SendToTerminalNMEA(MPU6000Data* mpu6000Data);


#endif /* USART_H_ */
