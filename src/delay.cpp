/*
 * delay.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#include "inc/delay.h"


volatile uint32_t uwTick;

void Timer_InitTick(uint32_t TickDiv, uint32_t TickPriority)
{
  SysTick_Config(SystemCoreClock / TickDiv);
  NVIC_SetPriority(SysTick_IRQn, TickPriority);
}


void Timer_IncTick(void)
{
  uwTick++;
}

uint32_t Timer_GetTick(void)
{
  return uwTick;
}

void Delay(uint32_t time)
{
	uint32_t startTime = Timer_GetTick();
	while((Timer_GetTick() - startTime) < time);
}


