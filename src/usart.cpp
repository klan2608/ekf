#include "inc/usart.h"
#include "inc/mpu6000.h"
#include "stm32f4xx_gpio.h"
#include "inc/led.h"

char dataForSend[200];
volatile uint8_t modeSender = 0;

void USART2Init(void)
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;
	USART_Init(USART2, &USART_InitStructure);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVIC_EnableIRQ(USART2_IRQn);
	USART_Cmd(USART2, ENABLE);

	UsartDmaInit();
}

void SendToTerminalNMEA(MPU6000Data* mpu6000Data)
{
	memset(dataForSend, 0, sizeof(dataForSend));

	switch (modeSender)
	{
	//only gyro
	case 1:
		sprintf(dataForSend, "$GBGYR,%f,%f,%f*00\n", mpu6000Data->Gyro.X,
				mpu6000Data->Gyro.Y, mpu6000Data->Gyro.Z);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
		//only accel
	case 2:
		sprintf(dataForSend, "$GBACC,%f,%f,%f*00\n", mpu6000Data->Accel.X,
				mpu6000Data->Accel.Y, mpu6000Data->Accel.Z);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
		//only MAG
	case 3:
		sprintf(dataForSend, "$GBMAG,%f,%f,%f*00\n", mpu6000Data->Mag.X,
				mpu6000Data->Mag.Y, mpu6000Data->Mag.Z);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
		//only angle
	case 4:
		sprintf(dataForSend, "$GBANG,%f,%f,%f*00\n", mpu6000Data->Mag.X,
				mpu6000Data->roll, mpu6000Data->pitch, mpu6000Data->yaw);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
		//only Time
	case 5:
		sprintf(dataForSend, "$GBTMR,%d*00\n", time);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
	default:
		sprintf(dataForSend,
				"$GBGYR,%f,%f,%f*00\n$GBACC,%f,%f,%f*00\n$GBMAG,%f,%f,%f*00\n$GBTMP,%f*00\n$GBTMR,%d*00\n$GBANG,%f,%f,%f*00\n",
				mpu6000Data->Gyro.X, mpu6000Data->Gyro.Y, mpu6000Data->Gyro.Z,
				mpu6000Data->Accel.X, mpu6000Data->Accel.Y, mpu6000Data->Accel.Z,
				mpu6000Data->Mag.X, mpu6000Data->Mag.Y, mpu6000Data->Mag.Z,
				mpu6000Data->Temp, time,
				mpu6000Data->roll, mpu6000Data->pitch, mpu6000Data->yaw);
		DMA_Cmd(DMA1_Stream6, ENABLE);
		break;
	}

//	if(mpu6000Data.Mag.X > 2.f || mpu6000Data.Mag.X < -2.f ||
//			mpu6000Data.Mag.Y > 2.f || mpu6000Data.Mag.Y < -2.f ||
//			mpu6000Data.Mag.Z > 2.f || mpu6000Data.Mag.Z < -2.f)
//	{
//		GPIO_ToggleBits(GPIOC, GPIO_Pin_10);
//	}
}

void SendToTerminal(MPU6000Data* mpu6000Data)
{
	memset(dataForSend, 0, sizeof(dataForSend));
	sprintf(dataForSend,
			"RAW Gyros: %f\t%f\t%f\tAccels: %f\t%f\t%f\tMag: %f\t%f\t%f\tTemp: %f\tTimer: %d\n",
			mpu6000Data->Gyro.X, mpu6000Data->Gyro.Y, mpu6000Data->Gyro.Z,
			mpu6000Data->Accel.X, mpu6000Data->Accel.Y, mpu6000Data->Accel.Z,
			mpu6000Data->Mag.X, mpu6000Data->Mag.Y, mpu6000Data->Mag.Z,
			mpu6000Data->Temp, 100);

//	sprintf(dataForSend, "Accels: %f\t%f\t%f\tGyro: %f\t%f\t%f\tMag: %f\t%f\t%f\tTemp: %f\tTimer: %d\n",
//			mpu6000Data.Accel.X, mpu6000Data.Accel.Y, mpu6000Data.Accel.Z,
//			mpu6000Data.Gyro.X, mpu6000Data.Gyro.Y, mpu6000Data.Gyro.Z,
//			mpu6000Data.Mag.X, mpu6000Data.Mag.Y, mpu6000Data.Mag.Z,
//			mpu6000Data.Temp, 0);
//	uint32_t size = (uint32_t) strlen(dataForSend);
//	DMA_SetCurrDataCounter(DMA1_Stream6, 0);
//	DMA_Cmd(DMA1_Stream6, ENABLE);

	for (uint8_t i = 0; i < sizeof(dataForSend); i++)
	{
		if (dataForSend[i] == 0x00)
			break;
		USART_SendData(USART2, dataForSend[i]);
		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET)
			;
	}
}

void SendAngles(float roll, float pitch, float yaw)
{

}


void UsartDmaInit(void)
{
	//Channel 4 Stream 6 (USART2_TX)
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	DMA_DeInit(DMA1_Stream6);

	DMA_InitTypeDef DMA_InitStructure;
	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(dataForSend);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &dataForSend;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART2->DR);
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_Init(DMA1_Stream6, &DMA_InitStructure);

	USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
	DMA_ITConfig(DMA1_Stream6, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA1_Stream6, DISABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Stream6_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}
