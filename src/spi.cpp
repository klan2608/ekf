/*
 * spi.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */

#include "inc/spi.h"


void Spi1Init()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = SPI_CLK_PIN | SPI_MOSI_PIN | SPI_MISO_PIN;
	GPIO_Init(SPI_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource3, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_SPI1);

	SPI_InitTypeDef SPI_InitStructure;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_CRCPolynomial = 1;
	SPI_Init(SPI1, &SPI_InitStructure);

	SPI_Cmd(SPI1, ENABLE);
//	SPI_CS_Cmd(ENABLE);
}


uint8_t SpiSendByte(SPI_TypeDef* spi,uint8_t byteToSend, uint32_t timeOut)
{
	uint32_t startTime = Timer_GetTick();
	while ((SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET) && ((Timer_GetTick() - startTime) < timeOut));
	volatile uint32_t stopTime = Timer_GetTick();
	SPI_I2S_SendData(spi, byteToSend);
	startTime = Timer_GetTick();
	while ((SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_RXNE) == RESET) && ((Timer_GetTick() - startTime) < timeOut));
	tempAnswerSPI = (uint8_t)SPI_I2S_ReceiveData(spi);
	return tempAnswerSPI;
}

uint8_t SpiReadByte(SPI_TypeDef* spi, uint32_t timeOut)
{
	return SpiSendByte(spi, 0xAA, timeOut);
}

void SPI_WriteData(SPI_TypeDef *spi, uint8_t addr, uint8_t data)
{
	SPI_CS_Cmd(ENABLE);
    SpiSendByte(spi, addr, SPI_TIME_OUT);
	SpiSendByte(spi, data, SPI_TIME_OUT);
	SPI_CS_Cmd(DISABLE);
}

uint8_t SPI_ReadData(SPI_TypeDef *spi, uint8_t addr)
{
	uint8_t dataSend = 0;
	uint8_t dataRead;
	dataSend = addr | 0x80;
	SPI_CS_Cmd(ENABLE);
	SpiSendByte(spi, dataSend, SPI_TIME_OUT);
	dataRead = SpiReadByte(spi, SPI_TIME_OUT);
	SPI_CS_Cmd(DISABLE);
	return dataRead;
}

void SPI_CS_Cmd(FunctionalState state)
{
	if(state == ENABLE)
		GPIO_ResetBits(SPI_CS_PORT, SPI_CS_PIN);
	else
		GPIO_SetBits(SPI_CS_PORT, SPI_CS_PIN);
}
