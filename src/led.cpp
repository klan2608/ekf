/*
 * led.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: Y. Reznikov
 */

#include "inc/led.h"

void LedInit()
{
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Pin = LED_1_PIN;
	GPIO_Init(LED_1_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED_2_PIN;
	GPIO_Init(LED_2_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED_3_PIN;
	GPIO_Init(LED_3_PORT, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = LED_4_PIN;
	GPIO_Init(LED_4_PORT, &GPIO_InitStructure);

	GPIO_SetBits(LED_1_PORT, LED_1_PIN);
	GPIO_SetBits(LED_2_PORT, LED_2_PIN);
	GPIO_SetBits(LED_3_PORT, LED_3_PIN);
	GPIO_SetBits(LED_4_PORT, LED_4_PIN);
}


