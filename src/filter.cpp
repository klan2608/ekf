/*
 * filter.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: Y.Reznikov
 */
#include "inc/filter.h"
#include "inc/mpu6000.h"

FLOAT_TYPE UAV_Euler[3];
FilterData EKFdata;
float Lambda[4];
float UAVroll, UAVpitch, UAVyaw;
extern MPU6000Data mpu6000Data;
float MagneticDeclanation = 5.4f;

FLOAT_TYPE CurrentMagCourseCompensatedRad()
{
    static FLOAT_TYPE UAV_Acceleration[3];
    static FLOAT_TYPE CalcVector;
    static FLOAT_TYPE MagValueScaled[3];
    static FLOAT_TYPE cosRoll;
    static FLOAT_TYPE sinRoll;
    static FLOAT_TYPE cosPitch;
    static FLOAT_TYPE sinPitch;
    static FLOAT_TYPE Xh;
    static FLOAT_TYPE Yh;
    static FLOAT_TYPE heading;


    UAV_Acceleration [0] = mpu6000Data.Accel.X;
    UAV_Acceleration [1] = mpu6000Data.Accel.Y;
    UAV_Acceleration [2] = mpu6000Data.Accel.Z;

    CalcVector = sqrt( UAV_Acceleration[0] * UAV_Acceleration[0] +
                       UAV_Acceleration[1] * UAV_Acceleration[1] +
                       UAV_Acceleration[2] * UAV_Acceleration[2] );
    UAV_Acceleration[0] /= CalcVector;
    UAV_Acceleration[1] /= CalcVector;
    UAV_Acceleration[2] /= CalcVector;

    MagValueScaled[0] = mpu6000Data.Mag.X;
    MagValueScaled[1] = mpu6000Data.Mag.Y;
    MagValueScaled[2] = mpu6000Data.Mag.Z;

    //Calc vars
    cosRoll = sqrt ( fabs( 1.0f - UAV_Acceleration[1] * UAV_Acceleration[1] ) ); //cheat abs
    sinRoll = UAV_Acceleration[1];
    cosPitch = sqrt ( fabs( 1.0f - UAV_Acceleration[0] * UAV_Acceleration[0] ) ); //cheat abs
    sinPitch = UAV_Acceleration[0];
    // The tilt compensation algorithm.
    Xh = MagValueScaled[0] * cosPitch + MagValueScaled[2] * sinPitch;
    Yh = MagValueScaled[0] * sinRoll * sinPitch + MagValueScaled[1] * cosRoll - MagValueScaled[2] * sinRoll * cosPitch;

    heading = - ( atan2 ( Yh, Xh ) - EKFdata.MagDecl_deg * Deg2Rad );

    if ( heading > M_PI ) {
        heading -= M_2_PI;
    }

    return heading;
}

void InitFilter()
{
	//TODO add check on GPS_VALID, else wait
	int i = 0;
	//Load Data
	EKFdata.GravityAccel_mps2 = G_EARTH;
	EKFdata.MagDecl_deg = 5.4; // Additional input needed to compute xdot, F, zhat, or H


	FLOAT_TYPE UAV_Acceleration[3] = {mpu6000Data.Accel.X,
									   mpu6000Data.Accel.Y,
									   mpu6000Data.Accel.Z};
	FLOAT_TYPE Lambda[4];

	FLOAT_TYPE CurrentSpeed[3] = {0.0, 0.0, 0.0};

	FLOAT_TYPE InitBiasAccel[3] = {0.0, 0.0, 0.0};
	FLOAT_TYPE InitBiasGyro[3] = {mpu6000Data.initX,
								  mpu6000Data.initY,
								  mpu6000Data.initZ};

	FLOAT_TYPE InitPosX[3];
	//-----dev EKF-----

	for (i = 0; i < 3; i++)
		InitPosX[i] = 0.0;

	//Roll
	UAV_Euler[1] = atan2 ( -UAV_Acceleration[1],-UAV_Acceleration[2] );
	//Pitch
	UAV_Euler[0] = -asin ( -UAV_Acceleration[0]/G_EARTH );
	//Yaw
	UAV_Euler[2] = CurrentMagCourseCompensatedRad();

	//-----------------????????? ???????? ??????????? ???????????-------------------------
	Lambda[0] = cos ( UAV_Euler[2]/2 ) *cos ( UAV_Euler[0]/2 ) *cos ( UAV_Euler[1]/2 ) + sin ( UAV_Euler[2]/2 ) *sin ( UAV_Euler[0]/2 ) *sin ( UAV_Euler[1]/2 );
	Lambda[1] = sin ( UAV_Euler[1]/2 ) *cos ( UAV_Euler[0]/2 ) *cos ( UAV_Euler[2]/2 ) - cos ( UAV_Euler[1]/2 ) *sin ( UAV_Euler[2]/2 ) *sin ( UAV_Euler[0]/2 );
	Lambda[2] = sin ( UAV_Euler[0]/2 ) *cos ( UAV_Euler[1]/2 ) *cos ( UAV_Euler[2]/2 ) + cos ( UAV_Euler[0]/2 ) *sin ( UAV_Euler[1]/2 ) *sin ( UAV_Euler[2]/2 );
	Lambda[3] = cos ( UAV_Euler[1]/2 ) *sin ( UAV_Euler[2]/2 ) *cos ( UAV_Euler[0]/2 ) - sin ( UAV_Euler[1]/2 ) *cos ( UAV_Euler[2]/2 ) *sin ( UAV_Euler[0]/2 );

	//Initial DataFrame insertion
	//-----dev EKF-----
	EKFdata.FillInitSigmas();

	EKFdata.SetInitX(Lambda,InitPosX,CurrentSpeed);
	EKFdata.SetInitBias(InitBiasGyro,InitBiasAccel);


	EKFdata.FillPQ();
}

void UpdateFilter ( FLOAT_TYPE secElapsed )
{
//------------------------------------------------------------------------
	EKFdata.SetNewGyroData(mpu6000Data.Gyro.X,
						   mpu6000Data.Gyro.Y,
						   mpu6000Data.Gyro.Z);

	EKFdata.SetNewAccelData(mpu6000Data.Accel.X,
						   mpu6000Data.Accel.Y,
						   mpu6000Data.Accel.Z);

	EKFdata.dt = secElapsed;
	EKFdata.FillZPos(0.0,0.0,0.0);
	EKFdata.FillZVelocity(0.0,0.0,0.0);

	if (mpu6000Data.DataReadyMag == 0) {
		EKFdata.EmptyZR();
	} else {
		EKFdata.FillZMag(mpu6000Data.Mag.X, mpu6000Data.Mag.Y, mpu6000Data.Mag.Z);
		EKFdata.FillR();
	}
	perform_ekf ( EKFdata );

	//------ ?????????????? ?????????? ??????? ???????????------------
	float sum = 0.0;
	for (uint8_t i=0; i<4; i++ )
		sum += EKFdata.xhat[i]*EKFdata.xhat[i];
	sum = sqrt ( sum );

	for (uint8_t i=0; i<4; i++ )
		EKFdata.xhat[i] /= sum;


	memcpy(Lambda,EKFdata.xhat,sizeof(FLOAT_TYPE) * 4);
	//----end dev EKF---

	//-------------------17------------------------
	//Roll
	UAVroll = atan2 ( 2 * ( Lambda[2]*Lambda[3] + Lambda[1]*Lambda[0]), 1 - 2 * ( Lambda[1]*Lambda[1] + Lambda[2]*Lambda[2] ) );
	mpu6000Data.roll = UAVroll * Rad2Deg;
	//Pitch
	UAVpitch = asin ( - 2 * ( Lambda[1]*Lambda[3] - Lambda[0]*Lambda[2] ) );
	mpu6000Data.pitch = UAVpitch * Rad2Deg;
	//Yaw
	UAVyaw = atan2 ( 2 * ( Lambda[1]*Lambda[2] + Lambda[0]*Lambda[3] ), 1 - 2 * ( Lambda[2]*Lambda[2] + Lambda[3]*Lambda[3] ) );
	mpu6000Data.yaw = UAVyaw * Rad2Deg;
}

