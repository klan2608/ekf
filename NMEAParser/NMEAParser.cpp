#include "NMEAParser.h"
#include <string.h>
#include <time.h>

#define KnotsToMPS 0.514

NMEAParser::NMEAParser()
{
	BufferSize = 0;
	ScanIndex = 0;
	ParseIndex = 0;
	MesStart = -1;
	MesEnd = -1;
	ParseIndex = MesEnd + 1;
	ScanIndex = MesEnd + 1;
	calibData.biasX = 0.f;
	calibData.scaleX = 1.f;
	calibData.biasY = 0.f;
	calibData.scaleY = 1.f;
	calibData.biasZ = 0.f;
	calibData.scaleZ = 1.f;
//	memset(&OutputStruct, 0, sizeof(OutputStruct));
	memset(TypeReady, 0, sizeof(TypeReady));
}

void NMEAParser::PutByte(uint8_t Byte)
{
	if (BufferSize == BufferMaxSize) {
		SafeDeleteParsed();
	}
	Buffer[BufferSize++] = Byte;
}

bool NMEAParser::IsAllDataRead()
{
	bool Out = true;
	for (int i = 0; i < NumberOfMessageTypes; i++)
		Out &= TypeReady[i];
	return Out;
}

void NMEAParser::ClearReadData()
{
	memset(TypeReady, 0, sizeof(TypeReady));
//	memset(&OutputStruct, 0, sizeof(OutputStruct));
//	SafeDeleteParsed();
	BufferSize = 0;
	ScanIndex = 0;
	ParseIndex = 0;
	MesStart = -1;
	MesEnd = -1;
	ParseIndex = 0;
	ScanIndex = 0;
}

void NMEAParser::NextMessage()
{
	ParseIndex = MesEnd + 1;
	ScanIndex = MesEnd + 1;
	MesStart = -1;
	MesEnd = -1;
}

void NMEAParser::SafeDeleteParsed()
{
	//http://habrahabr.ru/post/218451/
	//Если не катит, придётсья переносить поэлементно
	//Или отжалеть ещё один буфер, чтобы данные не затёрлись.
	//Что мы и сделаем.
	memcpy(BufferSafe,Buffer + ParseIndex,BufferMaxSize - ParseIndex);
	memcpy(Buffer, BufferSafe, BufferMaxSize - ParseIndex);
	MesStart -= ParseIndex;
	MesEnd -= ParseIndex;
	ValueStart -= ParseIndex;
	ValueEnd -= ParseIndex;
	ScanIndex -= ParseIndex;
	BufferSize -= ParseIndex;
	ParseIndex = 0;
}

void NMEAParser::Update()
{
	while (ScanIndex < BufferSize)
	{
		if (Buffer[ScanIndex] == '$')
		{ //found start
			MesStart = ScanIndex;
			MesEnd = -1;
		}
        if ((Buffer[ScanIndex] == '\n'))
		{
			MesEnd = ScanIndex;
		}
		ScanIndex++;
		if (!(MesStart < 0) && !(MesEnd < 0))
		{
			int8_t Type = GetMesType(MesStart, MesEnd);
			switch (Type) {
                case 0: //CLB
                    ParseCLB();
                    TypeReady[Type] = true;
                break;
			}
			NextMessage(); //AfterParsing or Type Error
		}
	}
}

int8_t NMEAParser::GetMesType(uint16_t Start, uint16_t)
{
	bool Correct;
	for (int i = 0; i < NumberOfMessageTypes; i++) {
		Correct = true;
		for (int j = 0; j < 3; j++) {
			Correct &= (Buffer[Start+3+j] == MessageTypes[i*3+j]);
		}
		if (Correct)
			return i;
	}
	return -1;
}


bool NMEAParser::GetNextValue()
{
    if (ValueEnd >= (MesEnd ) ) { // * F F \r
		ValueEnd = ValueStart = 0;
		return false;
	}
	ValueStart = ValueEnd + 2;
    for (int i = ValueStart; i < MesEnd; i++) {
		if ((Buffer[i] == ',') || (Buffer[i] == '*')) {
			ValueEnd = i - 1;
			return true;
		}
	}
	return false;
}

void NMEAParser::StartSearchValue()
{
	ValueStart = MesStart + 1; //Type
	for (int i = ValueStart; i < MesEnd - 4; i++) {
		if ((Buffer[i] == ',') || (Buffer[i] == '*')) {
			ValueEnd = i - 1;
			break;
		}
	}
}

void NMEAParser::ParseCLB()
{
    // "$GBCLB,0.000,0.000,0.000,0.000,0.000,0.000*00"
    //	sscanf(reinterpret_cast<char*>(Mybuff + MesStart), "$G%*4s,%*i,%*i,%i,", &satv);
    int8_t  ParamNo = 1;
    //	static uint8_t GSVCounter = 0;
    //	static uint8_t GSVCount = 0;
    //	int Temp;
    StartSearchValue();
    while (GetNextValue()) {
        if (Buffer[ValueStart] != ',') {
            switch (ParamNo) {
                case 1:
                	calibData.biasX = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
                case 2:
                	calibData.scaleX = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
                case 3:
                	calibData.biasY = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
                case 4:
                	calibData.scaleY = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
                case 5:
                	calibData.biasZ = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
                case 6:
                	calibData.scaleZ = Conv::StoF(Buffer+ValueStart,ValueEnd - ValueStart + 1);
                break;
            };
        }
        ParamNo++;
    }
}
