#ifndef NMEAPARSER_H_
#define NMEAPARSER_H_

#include "TypeConverter.h"


typedef struct
{
	float biasX;
	float scaleX;
	float biasY;
	float scaleY;
	float biasZ;
	float scaleZ;
} CalibrationData;

class NMEAParser {
    const static uint8_t NumberOfMessageTypes = 1;
	const static uint16_t BufferMaxSize = 512;
	bool TypeReady[NumberOfMessageTypes];
    const uint8_t MessageTypes[3 * NumberOfMessageTypes]  = { 'C', 'L', 'B'};
    uint8_t BufferSafe[BufferMaxSize];
	uint16_t BufferSize;
	uint16_t ScanIndex;
	uint16_t ParseIndex;
	int16_t ValueStart, ValueEnd;
	int MesStart, MesEnd;
	void SafeDeleteParsed();

	int8_t GetMesType(uint16_t Start, uint16_t End);
	bool GetNextValue();
	void StartSearchValue();
    void ParseCLB();
public:
	CalibrationData calibData;
    uint8_t Buffer[BufferMaxSize];

    NMEAParser();
	void PutByte(uint8_t Byte);
//    _gps_data GetGPSData();
	CalibrationData GetCalibData();
	bool IsAllDataRead();
	void ClearReadData();
	void NextMessage();
	void Update();
};

#endif /* NMEAPARSER_H_ */
