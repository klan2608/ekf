#ifndef STRCONV_H
#define STRCONV_H

#include <stdint.h>
#include <cmath>

namespace Conv
{

	static int32_t StoI(uint8_t *str, uint8_t size)
	{
		if (size == 0)
			return 0;

		int n, mul = 1;
		for (n = 0; n < size; n++) {
			if ((str[n] < '0' ) || (str[n] > '9')) {
				if ((str[n] != '-'))
					return 0;
			}
		}
		uint8_t SymCount = 0;
		if (*str == '-')
		{
			mul = -1;
			str++;
			size--;
		}
		for (n = 0; (*str >= '0') && (*str <= '9') && (SymCount < size);
			 SymCount++, str++)
			n = n * 10 + *str - '0';
		return n * mul;
	}

	static float StoF(uint8_t *FloatStr, uint8_t size)
	{

		if (size == 0)
			return 0;
		float CellPart, FractPart, div = 1;
		uint8_t i = 0;
		int8_t FrPos = -1;
		for (i = 0; i < size; i++)
			if (FloatStr[i] == '.') {
				FrPos = i;
				break;
			}
        bool WasMinus = (FloatStr[0] == '-');
		if (FrPos < 0) {
			//есть только целая часть
			return StoI(FloatStr, size);
		}	else {
			CellPart = StoI(FloatStr, FrPos); //вычисленная целая часть
			//рассмотрим дробную часть
			for (i = 0; i < size - FrPos - 1; i++)
				div *= 10.0;
			FractPart = StoI(FloatStr+FrPos+1, size - FrPos - 1) / div; //дробная часть
            if (WasMinus)
				return CellPart - FractPart;
			else
				return CellPart + FractPart;
		}
	}

	using namespace std;
	static uint8_t ItoS(int64_t in, char*FinalStr)
	{
        if (FinalStr==0) return 0;
		uint64_t temp;
		uint8_t Uplim = 20, Lowlim = 0, i = 0, k = 0;

		if (in < 0)
		{
			temp = abs(in);
			FinalStr[0] = '-';
			Lowlim = 1;
		}
		else
			temp = in;

		while (i <= Uplim)
		{
			if (temp % 10 != 0)
				k = i;
			temp /= 10;
			i++;
		}

		i = k + Lowlim;
		if (in < 0)
			temp = abs(in);
		else
			temp = in;

		while ((i >= Lowlim) && (i <= k + Lowlim))
		{
			FinalStr[i] = temp % 10 + '0';
			temp /= 10;
			i--;
		}

		FinalStr[k + 1] = '\0';
		return k + 1;
	}

	static uint8_t FtoS(float in, char*str, uint8_t pres)
	{

        if (str==0) return 0;
		uint8_t i=0,k=0,Lowlim=0;
		float ftemp;

		if (in<0)
		{
			in=fabs(in);
			str[0]='-';
			str++;
			Lowlim=1;
		}
		else ftemp=in;

		uint16_t temp=(uint16_t)in;
		uint8_t size1=ItoS(temp,str);

		ftemp=in-temp;
		str[size1]='.';
		size1++;
		while (i<=pres) 
		{
			ftemp*=10;
			if (((uint16_t)ftemp)%10!=0) k=i;
			i++;
		}
		i=Lowlim;

		ftemp=in-temp;
		while ((i>=Lowlim)&&(i<=k+Lowlim)) 
		{
			ftemp*=10;
			str[size1+i]=((uint16_t)ftemp%10)+'0';
			i++;
		}
		str[size1+i]='\0';
		return Lowlim+size1+i;
	}
}

#endif // STRCONV_H
