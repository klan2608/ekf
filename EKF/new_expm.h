#ifndef NEW_EXPM_H
#define NEW_EXPM_H

#include <cmath>
#include <string.h>
#include <stdint.h>
#include "macros.h"
#include "matrixoperations.h"

//pade approx - source: expokit - padm.m
// Общий случай достаточно тривиален, но не сильно оптимизирован по быстродействию.
// Для максимальной производительности было решено реализовать в хардкоде некоторые степени аппроксимации
// И задать их как параметры шаблона

namespace ExpmMath {

	template<uint8_t MSize, uint8_t PadeOrder,uint8_t SqScale> static void fexpm(FLOAT_TYPE* matrix, FLOAT_TYPE* result) {
		//ZERO result
		memset(result,0,sizeof(FLOAT_TYPE)*MSize*MSize);

		//if (!(PadeOrder & 1)) return; //ONLY ODD ORDER
		//Сами проверяйте порядок! =)

		constexpr int POW_2_N = 1 << SqScale;
		constexpr uint32_t SizeOfMatrixInBytes = MSize*MSize*sizeof(FLOAT_TYPE);
		static FLOAT_TYPE M_A[MSize*MSize];
		static FLOAT_TYPE M_P[MSize*MSize]; //FOR P
		static FLOAT_TYPE M_Q[MSize*MSize]; //FOR Q
		static int i, j, k, offset;
		static FLOAT_TYPE tmpF, tmpF2;
		//Zero temp
		memset(M_P,0,SizeOfMatrixInBytes);
		memset(M_Q,0,SizeOfMatrixInBytes);

		// Scaling
		//M_A = M/(2^N);
		for(i = 0; i < MSize*MSize; i++)
			M_A[i] = matrix[i] / POW_2_N;

		//Calc EXP
		switch (PadeOrder) {
			case 5: {
				//PadeCoefs
				static uint8_t ToInitPadeoCoefs = 1;
				static FLOAT_TYPE PadeCoef[PadeOrder+1];
				if (ToInitPadeoCoefs) {
					PadeCoef[0] = 1.0;
					for (int i = 1; i < PadeOrder+1; i++) {
						PadeCoef[i] = PadeCoef[i-1]*( (PadeOrder+1-i) / (FLOAT_TYPE)((i*(2*PadeOrder+1-i))));
					}
					ToInitPadeoCoefs = 0;
				}
				//Calc EXP
				static FLOAT_TYPE M_A2[MSize*MSize];
				static FLOAT_TYPE M_A4[MSize*MSize];
				/// A^2
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A,M_A,M_A2,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A2,M_A4,MSize);

				//Fill first P & Q from start
				for (i = 0; i < MSize; i++) {
					M_Q[i*MSize+i] = PadeCoef[1];
					M_P[i*MSize+i] = 1.0; //PadeCoef[0] = 1.0
				}
				//Add other power of 2
				// MAX SPEED!!! =)
				for (i = 0 ; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						M_Q[j+offset] += M_A2[j+offset]*PadeCoef[3]+M_A4[j+offset]*PadeCoef[5];
						M_P[j+offset] += M_A2[j+offset]*PadeCoef[2]+M_A4[j+offset]*PadeCoef[4];
					}
				}
				//Mult on A to get odd powers
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_Q, M_A, M_A2,MSize); //M_A2 = M_Q
				//Subtract
				MatrixOperations::subtractSquareMatrixes<FLOAT_TYPE>(M_A2,M_P,M_A2,MSize);
				//Divide - P/Q
				MatrixOperations::mldivideA<FLOAT_TYPE,MSize,SizeOfMatrixInBytes>(M_A2,M_P); //M_P = result
				//Finalize
				for (i = 0; i < MSize*MSize; i++)
					M_P[i] *= -2.0;
				for (i = 0; i < MSize; i++)
					M_P[i*MSize + i] += -1.0;
				//Final answer in M_P
				//End Calc exp.
				break;
			}
			case 7: {
				//PadeCoefs
				static uint8_t ToInitPadeoCoefs = 1;
				static FLOAT_TYPE PadeCoef[PadeOrder+1];
				if (ToInitPadeoCoefs) {
					PadeCoef[0] = 1.0;
					for (int i = 1; i < PadeOrder+1; i++) {
						PadeCoef[i] = PadeCoef[i-1]*( (PadeOrder+1-i) / (FLOAT_TYPE)((i*(2*PadeOrder+1-i))));
					}
					ToInitPadeoCoefs = 0;
				}
				//Calc EXP
				static FLOAT_TYPE M_A2[MSize*MSize];
				static FLOAT_TYPE M_A4[MSize*MSize];
				static FLOAT_TYPE M_A6[MSize*MSize];
				/// A^2
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A,M_A,M_A2,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A2,M_A4,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A4,M_A6,MSize);

				//Fill first P & Q from start
				for (i = 0; i < MSize; i++) {
					M_Q[i*MSize+i] = PadeCoef[1];
					M_P[i*MSize+i] = 1.0; //PadeCoef[0] = 1.0
				}
				//Add other power of 2
				// MAX SPEED!!! =)
				for (i = 0 ; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						M_Q[j+offset] += M_A2[j+offset]*PadeCoef[3]+M_A4[j+offset]*PadeCoef[5]+M_A6[j+offset]*PadeCoef[7];
						M_P[j+offset] += M_A2[j+offset]*PadeCoef[2]+M_A4[j+offset]*PadeCoef[4]+M_A6[j+offset]*PadeCoef[6];
					}
				}
				//Mult on A to get odd powers
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_Q, M_A, M_A2,MSize); //M_A2 = M_Q
				//Subtract
				MatrixOperations::subtractSquareMatrixes<FLOAT_TYPE>(M_A2,M_P,M_A2,MSize);
				//Divide - P/Q
				MatrixOperations::mldivideA<FLOAT_TYPE,MSize,SizeOfMatrixInBytes>(M_A2,M_P); //M_P = result
				//Finalize
				for (i = 0; i < MSize*MSize; i++)
					M_P[i] *= -2.0;
				for (i = 0; i < MSize; i++)
					M_P[i*MSize + i] += -1.0;
				//Final answer in M_P
				//End Calc exp.
				break;
			}
			case 9: {
				//PadeCoefs
				static uint8_t ToInitPadeoCoefs = 1;
				static FLOAT_TYPE PadeCoef[PadeOrder+1];
				if (ToInitPadeoCoefs) {
					PadeCoef[0] = 1.0;
					for (int i = 1; i < PadeOrder+1; i++) {
						PadeCoef[i] = PadeCoef[i-1]*( (PadeOrder+1-i) / (FLOAT_TYPE)((i*(2*PadeOrder+1-i))));
					}
					ToInitPadeoCoefs = 0;
				}
				//Calc EXP
				static FLOAT_TYPE M_A2[MSize*MSize];
				static FLOAT_TYPE M_A4[MSize*MSize];
				static FLOAT_TYPE M_Ptmp[MSize*MSize]; //FOR P
				static FLOAT_TYPE M_Qtmp[MSize*MSize]; //FOR Q
				/// A^2
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A,M_A,M_A2,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A2,M_A4,MSize);

				//Fill first P & Q from start
				for (i = 0; i < MSize; i++) {
					M_Q[i*MSize+i] = PadeCoef[1];
					M_P[i*MSize+i] = 1.0; //PadeCoef[0] = 1.0
					M_Qtmp[i*MSize+i] = PadeCoef[5];
					M_Ptmp[i*MSize+i] = PadeCoef[4];
				}
				//Add other power of 2
				// MAX SPEED!!! =)
				for (i = 0 ; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						M_Q[j+offset] += M_A2[j+offset]*PadeCoef[3];
						M_P[j+offset] += M_A2[j+offset]*PadeCoef[2];
						M_Qtmp[j+offset] += M_A2[j+offset]*PadeCoef[7]+M_A4[j+offset]*PadeCoef[9];
						M_Ptmp[j+offset] += M_A2[j+offset]*PadeCoef[6]+M_A4[j+offset]*PadeCoef[8];
					}
				}
				//Mult Temp and add To real
				for (i = 0; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						tmpF = 0.0;
						tmpF2 = 0.0;
						for (k = 0; k < MSize; k++) {
							tmpF += (M_Qtmp[k+offset]*M_A4[j + k*MSize]);
							tmpF2 += (M_Ptmp[k+offset]*M_A4[j + k*MSize]);
						}
						M_Q[offset+j] += tmpF;
						M_P[offset+j] += tmpF2;
					}
				}

				//Mult on A to get odd powers
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_Q, M_A, M_A2,MSize); //M_A2 = M_Q
				//Subtract
				MatrixOperations::subtractSquareMatrixes<FLOAT_TYPE>(M_A2,M_P,M_A2,MSize);
				//Divide - P/Q
				MatrixOperations::mldivideA<FLOAT_TYPE,MSize,SizeOfMatrixInBytes>(M_A2,M_P); //M_P = result
				//Finalize
				for (i = 0; i < MSize*MSize; i++)
					M_P[i] *= -2.0;
				for (i = 0; i < MSize; i++)
					M_P[i*MSize + i] += -1.0;
				//Final answer in M_P
				//End Calc exp.
				break;
			}
			case 13: {
				//PadeCoefs
				static uint8_t ToInitPadeoCoefs = 1;
				static FLOAT_TYPE PadeCoef[PadeOrder+1];
				if (ToInitPadeoCoefs) {
					PadeCoef[0] = 1.0;
					for (i = 1; i < PadeOrder+1; i++) {
						PadeCoef[i] = PadeCoef[i-1]*( (PadeOrder+1-i) / (FLOAT_TYPE)((i*(2*PadeOrder+1-i))));
					}
					ToInitPadeoCoefs = 0;
				}
				//Calc EXP
				static FLOAT_TYPE M_A2[MSize*MSize];
				static FLOAT_TYPE M_A4[MSize*MSize];
				static FLOAT_TYPE M_A6[MSize*MSize];
				static FLOAT_TYPE M_Ptmp[MSize*MSize]; //FOR P
				static FLOAT_TYPE M_Qtmp[MSize*MSize]; //FOR Q
				/// A^2
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A,M_A,M_A2,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A2,M_A4,MSize);
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A2,M_A4,M_A6,MSize);

				//Fill first P & Q from start
				for (i = 0; i < MSize; i++) {
					M_Q[i*MSize+i] = PadeCoef[1];
					M_P[i*MSize+i] = 1.0; //PadeCoef[0] = 1.0
					M_Qtmp[i*MSize+i] = PadeCoef[7];
					M_Ptmp[i*MSize+i] = PadeCoef[6];
				}
				//Add other power of 2
				// MAX SPEED!!! =)
				for (i = 0 ; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						M_Q[j+offset] += M_A2[j+offset]*PadeCoef[3]+M_A4[j+offset]*PadeCoef[5];
						M_P[j+offset] += M_A2[j+offset]*PadeCoef[2]+M_A4[j+offset]*PadeCoef[4];
						M_Qtmp[j+offset] += M_A2[j+offset]*PadeCoef[9]+M_A4[j+offset]*PadeCoef[11]+M_A6[j+offset]*PadeCoef[13];
						M_Ptmp[j+offset] += M_A2[j+offset]*PadeCoef[8]+M_A4[j+offset]*PadeCoef[10]+M_A6[j+offset]*PadeCoef[12];
					}
				}
				//Mult Temp and add To real
				for (i = 0; i < MSize; i++) {
					offset = i*MSize;
					for (j = 0; j < MSize; j++) {
						tmpF = 0.0;
						tmpF2 = 0.0;
						for (k = 0; k < MSize; k++) {
							tmpF += (M_Qtmp[k+offset]*M_A6[j + k*MSize]);
							tmpF2 += (M_Ptmp[k+offset]*M_A6[j + k*MSize]);
						}
						M_Q[offset+j] += tmpF;
						M_P[offset+j] += tmpF2;
					}
				}

				//Mult on A to get odd powers
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_Q, M_A, M_A2,MSize); //M_A2 = M_Q
				//Subtract
				MatrixOperations::subtractSquareMatrixes<FLOAT_TYPE>(M_A2,M_P,M_A2,MSize);
				//Divide - P/Q
				MatrixOperations::mldivideA<FLOAT_TYPE,MSize,SizeOfMatrixInBytes>(M_A2,M_P); //M_P = result
				//Finalize
				for (i = 0; i < MSize*MSize; i++)
					M_P[i] *= -2.0;
				for (i = 0; i < MSize; i++)
					M_P[i*MSize + i] += -1.0;
				//Final answer in M_P
				//End Calc exp.
				break;
			}
		}
		// Squaring step
		j = 0; //1 - answer in M_A,  0 - in M_P
		for(i = 0; i < SqScale; i++) {
			if (j)
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_A,M_A,M_P,MSize);
			else
				MatrixOperations::multSquareMatrixes<FLOAT_TYPE,SizeOfMatrixInBytes>(M_P,M_P,M_A,MSize);
			j = 1 - j;
		}
		if (j)
			memcpy(result,M_A,SizeOfMatrixInBytes);
		else
			memcpy(result,M_P,SizeOfMatrixInBytes);
	}
}
#endif // NEW_EXPM_H
