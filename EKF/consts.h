#ifndef CONSTS_H
#define CONSTS_H

static const float G_EARTH = 9.80665f; //ускорение свободного падения у поверхности Земли
static const float Rad2Deg = 57.29577951308232f;           // Radians to degrees conversion factor
static const float Deg2Rad = 0.01745329251994f;          // Degrees to radians conversion factor

#endif // CONSTS_H
