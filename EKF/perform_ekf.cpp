#include "compute_xdot_and_F__ins_ekf_quaternion.h"
#include "compute_zhat_and_H__ins_ekf_quaternion.h"
#include "perform_ekf.h"
#include "ekfTypes.h"

#include "matrixoperations.h"
#include "new_expm.h"

#define MATRIXSIZE 32

void perform_ekf(FilterData& InputData)
{
    int i;

	static FLOAT_TYPE AA[MATRIXSIZE*MATRIXSIZE]; // 32x32
	static FLOAT_TYPE BB[MATRIXSIZE*MATRIXSIZE]; // 32x32

    static FLOAT_TYPE F[MATRIXSIZE*MATRIXSIZE/4];
    static FLOAT_TYPE xdot[MATRIXSIZE / 2];
	static FLOAT_TYPE K[MATRIXSIZE/2 * 9];

    static FLOAT_TYPE H[9*MATRIXSIZE/2];
    static FLOAT_TYPE zhat[9];

	//Temps
	static FLOAT_TYPE tempHalfMatrix1[MATRIXSIZE*MATRIXSIZE/4]; // 16x16
	static FLOAT_TYPE tempHalfMatrix2[MATRIXSIZE*MATRIXSIZE/4];
	static FLOAT_TYPE tempHalfMatrix3[MATRIXSIZE*MATRIXSIZE/4];


	static FLOAT_TYPE P_k_Mult_H_Transponse[MATRIXSIZE/2 * 9];
	static FLOAT_TYPE temp_Div[9 * 9];
	static FLOAT_TYPE temp_Div2[9 * 9];

	static FLOAT_TYPE b_z_k[9];
	static FLOAT_TYPE K_Mult_z[MATRIXSIZE/2];
	compute_xdot_and_F__ins_ekf_quaternion(InputData, xdot, F);

    /*  Use VanLoan Method to convert from linearized continuous-domain state  */
    /*  model (F,Q_t), into discrete-domain state transformation (PHI_k & Q_k). */
    /*  Thus, assuming u(t) is relatively constant between times k and k+1, it  */
    /*  converts */
    /*    xdot(t)=F*x(t)+Fu*u(t)+w(t), E[w(t)*w(t)']=Q(t)  */
    /*  to */
    /*    x(k+1)=PHI(k)*x(k)+PHIu(k)*u(k)+w(k), E[w(k)*w(k)']=Q(k). */
    /*  */
    /*  VanLoan Method: */
    /*  */
    /*        [ -F(t) | Q(t) ] */
    /*   AA = [-------+------]*dt */
    /*        [  zero | F(t)'] */
    /*  */
    /*                   [ ... | inv(PHI(k))*Q(k)] */
    /*   BB = expm(AA) = [-----+-----------------] */
    /*                   [ zero|      PHI(k)'    ] */
    /*  */
    /*  Thus, PHI(k) is the transpose of the lower-right of BB.  Using the upper */
    /*  right of BB, Q(k) = PHI(k) * inv(PHI(k))*Q(k).  The derivation of PHIu is  */
    /*  not provided because it is not needed. */


    memset(AA,0,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE); // AA = 0
    // Fill -F(t)
    MatrixOperations::copySquareToPartOfSquare<FLOAT_TYPE>(AA,F,MATRIXSIZE,MATRIXSIZE/2,0,0);
    MatrixOperations::multPart<FLOAT_TYPE>(AA,MATRIXSIZE,MATRIXSIZE/2,MATRIXSIZE/2,0,0,-1.0);
    //Fill Q(t)
	MatrixOperations::copySquareToPartOfSquare<FLOAT_TYPE>(AA,InputData.Q,MATRIXSIZE,MATRIXSIZE/2,0,MATRIXSIZE/2);
    //Fill F(t)'
    MatrixOperations::copyTranposingSquareToPartOfSquare<FLOAT_TYPE>(AA,F,MATRIXSIZE,MATRIXSIZE/2,MATRIXSIZE/2,MATRIXSIZE/2);
	//We don't need F from now
    //Multiply dt
	MatrixOperations::multPart<FLOAT_TYPE>(AA,MATRIXSIZE,MATRIXSIZE,MATRIXSIZE,0,0,InputData.dt);

    /*  <- Matrix exponential! */
	//При меньшем порядке набегает слишком большая ошибка!
	//не использовать меньший порядок!!!
	ExpmMath::fexpm<MATRIXSIZE,13,1>(AA, BB);

    /* 'perform_ekf:228' PHI_k = BB(nStates+1:2*nStates,nStates+1:2*nStates)'; */
	MatrixOperations::copyTransposingPartOfSquareToSquare<FLOAT_TYPE>(tempHalfMatrix1,BB,MATRIXSIZE/2,MATRIXSIZE,MATRIXSIZE/2,MATRIXSIZE/2); // tempHalfMatrix1 = PHI_k
    /* 'perform_ekf:229' Q_k = PHI_k*BB(1:nStates,nStates+1:2*nStates); */
	MatrixOperations::copyPartOfSquareToSquare<FLOAT_TYPE>(tempHalfMatrix2,BB,MATRIXSIZE/2,MATRIXSIZE,0,MATRIXSIZE/2);
	MatrixOperations::multSquareMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4>(tempHalfMatrix1,tempHalfMatrix2,F,MATRIXSIZE/2); //F = Q_k now

    /*  Predict state estimate xhat forward dt seconds. */
    /*  */
    /*  We use Euler integration on the non-linear state derivatives,  */
    /*  xdot(t)=f(x(t),u(t)), to propagate xhat dt seconds from time k to time */
    /*  k+1: */
    /*     xhat(k+1) = xhat(k) + xdot(t)*dt. */
    /*  We alternatively could have used: */
    /*     xhat(k+1) = PHI*xhat(k) + PHIu*u(k). */
    /*  Doing so, however, would have introduced some linearization error and */
    /*  required us to compute PHIu. */

    /* 'perform_ekf:241' xhat_k = xhat_k + xdot*dt; */
    for(i=0; i<MATRIXSIZE/2; i++)
		InputData.xhat[i] += xdot[i] * InputData.dt;

    /*  Predict state covariance (P_k) forward dt seconds. */
    /*    PHI_k: State transition matrix from time k to time k+1 */
    /*    Q_k:   Discrete-time process noise matrix */
    /*  */
    /*  Note: In the P_k equation below, the PHI_k*P_k*PHI_k' term directly */
    /*  propagates the "a priori" state uncertainty from time k to time k+1.  The */
    /*  Q_k term adds additional uncertainty at each time step due to the process */
    /*  noise, w(k). */
    /*     P(k+1) = PHI(k)*P(k)*PHI(k)' + Q(k) */
    /* 'perform_ekf:252' P_k = PHI_k*P_k*PHI_k' + Q_k; */

	MatrixOperations::multSquareMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4>(tempHalfMatrix1,InputData.P,tempHalfMatrix2,MATRIXSIZE/2);
	MatrixOperations::multSquareMatrixesTransposingRight<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4>(tempHalfMatrix2,tempHalfMatrix1,tempHalfMatrix3,MATRIXSIZE/2);
	MatrixOperations::addSquareMatrixes<FLOAT_TYPE>(tempHalfMatrix3,F,InputData.P,MATRIXSIZE/2); //after that don't need Q_k

    bool z_empty = true;
	for (i = 0; i < 9; i++) {
		if (InputData.z[i] != 0.0) {
            z_empty = false;
            break;
        }
    }
    if (!z_empty) // -----New measurments------
    {
		compute_zhat_and_H__ins_ekf_quaternion(InputData, zhat, H);

        /*  K: Kalman gain matrix */
        /*   K = P_k*H'*inv(H*P_k*H'+R_k); */
        /*  */
        /*  Note: in Matlab, inv() can be very slow and inaccurate, so we'll use */
        /*  the forward-slash ("/", see "help mrdivide") instead.   */
        /*  See Reference 4 for a potentially more efficient Cholesky  */
        /*  Factorization method for computing K, xhat_k & P_k. */
        /* 'perform_ekf:273' K = (P_k*H')/(H*P_k*H'+R_k); */

		MatrixOperations::multMatrixesRightTransponse<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE/2*9>(InputData.P,H,P_k_Mult_H_Transponse,MATRIXSIZE/2,MATRIXSIZE/2,9);
		MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*9*9>(H,P_k_Mult_H_Transponse,temp_Div,9,MATRIXSIZE/2,9);
		MatrixOperations::addSquareMatrixes<FLOAT_TYPE>(temp_Div, InputData.R ,temp_Div2,9);
		MatrixOperations::invSquareMatrix<FLOAT_TYPE,9,sizeof(FLOAT_TYPE)*9*9>(temp_Div2,temp_Div); // tempDiv = 1/(H*P_k*H'+R_k);

		MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE/2*9>(P_k_Mult_H_Transponse,temp_Div,K,MATRIXSIZE/2,9,9);

        /*  Update of state estimates using measurement residual (z-zhat). */
        /*  */
        /*  Effectively, K provides a means weighting how much the measurement */
        /*  should affect the state estimate, xhat.  Thus, if the measurement  */
        /*  uncertainty (R_k) is "large" compared with the state uncertainty (P_k) */
        /*  then gain matrix K will be "small", meaning that the new measurement  */
        /*  will have little effect on the state estimate. */
        /*  In contrast, if the measurement uncertainty is small, then K will */
        /*  be large meaning we have high confidence in the measurement (z_k) and */
        /*  will weight it more than the predicted state estimate. */
        /* 'perform_ekf:284' xhat_k = xhat_k + K*(z_k-zhat); */
        for (i = 0; i < 9; i++) {
			b_z_k[i] = InputData.z[i] - zhat[i];
        }
		MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE/2*1>(K,b_z_k,K_Mult_z,MATRIXSIZE/2,9,1);
        for (i = 0; i < MATRIXSIZE/2; i++)
			InputData.xhat[i] += K_Mult_z[i];


        /*  Update of state covariance matrix */
        /*  */
        /*  Effectively, this update reduces the state uncertainties due to  */
        /*  information gained in the measurement. */
        /*    P(k+1) = (I - K*H)*P(k+1)   (Note: I is the identity matrix) */
        /*           = P(k+1) - K*H*P(k+1) */

        /* 'perform_ekf:292' P_k = (eye(length(xhat_k))-K*H)*P_k; */

		memset(tempHalfMatrix1,0,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4);
        for (i = 0; i < MATRIXSIZE/2; i++)
			tempHalfMatrix1[i + i*MATRIXSIZE/2] = 1;

		MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4>(K,H,tempHalfMatrix2,MATRIXSIZE/2,9,MATRIXSIZE/2);
		MatrixOperations::multPart<FLOAT_TYPE>(tempHalfMatrix2,MATRIXSIZE/2,MATRIXSIZE/2,MATRIXSIZE/2,0,0,-1.0);
		MatrixOperations::addSquareMatrixes<FLOAT_TYPE>(tempHalfMatrix1,tempHalfMatrix2,tempHalfMatrix3,MATRIXSIZE/2);
		MatrixOperations::multSquareMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4>(tempHalfMatrix3,InputData.P,tempHalfMatrix1,MATRIXSIZE/2);
		memcpy(InputData.P,tempHalfMatrix1,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4);
    }

    /*  Assign output values. */
    /*  These should be inputted back into perform_ekf at the subsequent time  */
    /*  step. */
    /* 'perform_ekf:299' xhat_new=xhat_k; */
	//memcpy(xhat_new,InputData.xhat,sizeof(FLOAT_TYPE)*MATRIXSIZE/2);
    /* P_new   =P_k;    % P(k+1) */
	//memcpy(P_new,InputData.P,sizeof(FLOAT_TYPE)*MATRIXSIZE*MATRIXSIZE/4);

}
