#ifndef MACROS_H
#define MACROS_H

#ifdef _MORE_PRECISION
	#define FLOAT_TYPE double
#else
	#define FLOAT_TYPE float
#endif

#endif // MACROS_H
