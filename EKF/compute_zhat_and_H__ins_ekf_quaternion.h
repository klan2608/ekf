/*
 * compute_zhat_and_H__ins_ekf_quaternion.h
 *
 * Code generation for function 'compute_zhat_and_H__ins_ekf_quaternion'
 *
 * C source code generated on: Fri Apr 11 14:11:48 2014
 *
 */

#ifndef __COMPUTE_ZHAT_AND_H__INS_EKF_QUATERNION_H__
#define __COMPUTE_ZHAT_AND_H__INS_EKF_QUATERNION_H__
/* Include files */
#include <cmath>
#include <stddef.h>
//#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "ekfTypes.h"
#include "macros.h"

extern void compute_zhat_and_H__ins_ekf_quaternion(FilterData& EKFdata, FLOAT_TYPE zhat[9], FLOAT_TYPE H[144]);
#endif
