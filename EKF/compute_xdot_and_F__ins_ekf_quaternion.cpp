#include "compute_xdot_and_F__ins_ekf_quaternion.h"
#include "compute_zhat_and_H__ins_ekf_quaternion.h"
#include "perform_ekf.h"
#include "matrixoperations.h"

void compute_xdot_and_F__ins_ekf_quaternion(FilterData &EKFdata, FLOAT_TYPE xdot[], FLOAT_TYPE F[])
{
	static int i;
	static FLOAT_TYPE M[9]; // Матрица перехода из собств. в географ.
	static FLOAT_TYPE g[3] = {0.0, 0.0, EKFdata.GravityAccel_mps2}; // Вектор ускорения свободного падения в географ.
	static FLOAT_TYPE C_bodyrate2qdot[12];
	static FLOAT_TYPE compl_gyro[3]; // Комплексированные значения гироскопа
	static FLOAT_TYPE compl_accel[3]; // Комплексированные значения акселлерометра
	static FLOAT_TYPE * x;

	x = EKFdata.xhat;
	// .5*-q1  -q2  -q3; ...
    //     q0  -q3   q2; ...
    //     q3   q0  -q1; ...
    //    -q2   q1   q0
	C_bodyrate2qdot[0] = -x[1];
    C_bodyrate2qdot[1] = -x[2];
    C_bodyrate2qdot[2] = -x[3];
    C_bodyrate2qdot[3] = x[0];
    C_bodyrate2qdot[4] = -x[3];
    C_bodyrate2qdot[5] = x[2];
    C_bodyrate2qdot[6] = x[3];
    C_bodyrate2qdot[7] = x[0];
    C_bodyrate2qdot[8] = -x[1];
    C_bodyrate2qdot[9] = -x[2];
    C_bodyrate2qdot[10] = x[1];
	C_bodyrate2qdot[11] = x[0];
	for (i=0;i<12;i++)
		C_bodyrate2qdot[i]*=0.5; 

	//Transponse M
    M[0] = 1.0 - 2.0 * (x[2]*x[2] + x[3]*x[3]);
    M[3] = 2.0 * (x[1] * x[2] + x[3] * x[0]);
    M[6] = 2.0 * (x[1] * x[3] - x[2] * x[0]);
    M[1] = 2.0 * (x[1] * x[2] - x[3] * x[0]);
    M[4] = 1.0 - 2.0 * (x[1]*x[1] + x[3]*x[3]);
    M[7] = 2.0 * (x[2] * x[3] + x[1] * x[0]);
    M[2] = 2.0 * (x[1] * x[3] + x[2] * x[0]);
    M[5] = 2.0 * (x[2] * x[3] - x[1] * x[0]);
    M[8] = 1.0 - 2.0 * (x[1]*x[1] + x[2]*x[2]);
	
	for (i = 0; i < 3; i++) {
		compl_gyro[i] = EKFdata.gyro_rps[i] - EKFdata.xhat[10+i];
		compl_accel[i] = EKFdata.accel_mps2[i] - EKFdata.xhat[13+i];
	}

    memset(xdot,0,sizeof(FLOAT_TYPE)*16);
    //q0 , q1, q2, q3
	MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*4*1>(C_bodyrate2qdot,compl_gyro,xdot,4,3,1);
    //Vn; Ve; -Vd
    xdot[4] = x[7];
    xdot[5] = x[8];
    xdot[6] = -x[9];
	MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*3*1>(M,compl_accel,xdot+7,3,3,1);

	for (i = 0; i < 3; i++)
		xdot[7+i] += g[i];


    /*  Compute linearized state dynamics, F = d(xdot)/dx */
	/* F = [ ... */
	/* [                                 0,                                        bwx/2 - wx/2,                                        bwy/2 - wy/2,                                        bwz/2 - wz/2, 0, 0, 0, 0, 0,  0,  q1/2,  q2/2,  q3/2,                   0,                   0,                   0] */
	/* [                      wx/2 - bwx/2,                                                   0,                                        wz/2 - bwz/2,                                        bwy/2 - wy/2, 0, 0, 0, 0, 0,  0, -q0/2,  q3/2, -q2/2,                   0,                   0,                   0] */
	/* [                      wy/2 - bwy/2,                                        bwz/2 - wz/2,                                                   0,                                        wx/2 - bwx/2, 0, 0, 0, 0, 0,  0, -q3/2, -q0/2,  q1/2,                   0,                   0,                   0] */
	/* [                      wz/2 - bwz/2,                                        wy/2 - bwy/2,                                        bwx/2 - wx/2,                                                   0, 0, 0, 0, 0, 0,  0,  q2/2, -q1/2, -q0/2,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 1, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 1,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0, -1,     0,     0,     0,                   0,                   0,                   0] */
	/* [ 2*q3*(bay - fy) - 2*q2*(baz - fz),                 - 2*q2*(bay - fy) - 2*q3*(baz - fz), 4*q2*(bax - fx) - 2*q1*(bay - fy) - 2*q0*(baz - fz), 2*q0*(bay - fy) + 4*q3*(bax - fx) - 2*q1*(baz - fz), 0, 0, 0, 0, 0,  0,     0,     0,     0, 2*q2^2 + 2*q3^2 - 1,   2*q0*q3 - 2*q1*q2, - 2*q0*q2 - 2*q1*q3] */
	/* [ 2*q1*(baz - fz) - 2*q3*(bax - fx), 4*q1*(bay - fy) - 2*q2*(bax - fx) + 2*q0*(baz - fz),                 - 2*q1*(bax - fx) - 2*q3*(baz - fz), 4*q3*(bay - fy) - 2*q0*(bax - fx) - 2*q2*(baz - fz), 0, 0, 0, 0, 0,  0,     0,     0,     0, - 2*q0*q3 - 2*q1*q2, 2*q1^2 + 2*q3^2 - 1,   2*q0*q1 - 2*q2*q3] */
	/* [ 2*q2*(bax - fx) - 2*q1*(bay - fy), 4*q1*(baz - fz) - 2*q3*(bax - fx) - 2*q0*(bay - fy), 2*q0*(bax - fx) - 2*q3*(bay - fy) + 4*q2*(baz - fz),                 - 2*q1*(bax - fx) - 2*q2*(bay - fy), 0, 0, 0, 0, 0,  0,     0,     0,     0,   2*q0*q2 - 2*q1*q3, - 2*q0*q1 - 2*q2*q3, 2*q1^2 + 2*q2^2 - 1] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */
	/* [                                 0,                                                   0,                                                   0,                                                   0, 0, 0, 0, 0, 0,  0,     0,     0,     0,                   0,                   0,                   0] */

    memset(F,0,sizeof(FLOAT_TYPE)*16*16);

    //TODO 2.0 out of brackets
    // Row 0
	F[1] = x[10] / 2.0 - EKFdata.gyro_rps[0] / 2.0;
	F[2] = x[11] / 2.0 - EKFdata.gyro_rps[1] / 2.0;
	F[3] = x[12] / 2.0 - EKFdata.gyro_rps[2] / 2.0;

    F[10] = x[1] / 2.0;
    F[11] = x[2] / 2.0;
    F[12] = x[3] / 2.0;

    // Row 1
	F[16] = (EKFdata.gyro_rps[0] - x[10]) / 2.0;
	F[18] = EKFdata.gyro_rps[2] / 2.0 - x[12] / 2.0;
	F[19] = x[11] / 2.0f - EKFdata.gyro_rps[1] / 2.0;

    F[26] = -x[0] / 2.0;
    F[27] = x[3] / 2.0;
    F[28] = -x[2] / 2.0;
	
    //Row 2
	F[32] = EKFdata.gyro_rps[1] / 2.0 - x[11] / 2.0;
	F[33] = x[12] / 2.0 - EKFdata.gyro_rps[2] / 2.0;
	F[35] = EKFdata.gyro_rps[0] / 2.0 - x[10] / 2.0;

    F[42] = -x[3] / 2.0;
    F[43] = -x[0] / 2.0;
    F[44] = x[1] / 2.0;

    //Row 3
	F[48] = EKFdata.gyro_rps[2] / 2.0f - x[12] / 2.0;
	F[49] = EKFdata.gyro_rps[1] / 2.0f - x[11] / 2.0;
	F[50] = x[10] / 2.0f - EKFdata.gyro_rps[0] / 2.0;

    F[58] = x[2] / 2.0;
    F[59] = -x[1] / 2.0;
    F[60] = -x[0] / 2.0;

    //Row 4
    F[71] = 1.0;

    //Row 5
    F[88] = 1.0;

    //Row 6
    F[105] = -1.0;

    //Row 7
	F[112] = 2.0 * x[3] * (x[14] - EKFdata.accel_mps2[1]) - 2.0 * x[2] * (x[15] -
		EKFdata.accel_mps2[2]);
	F[113] = -2.0 * x[2] * (x[14] - EKFdata.accel_mps2[1]) - 2.0 * x[3] * (x[15] -
		EKFdata.accel_mps2[2]);
	F[114] = (4.0 * x[2] * (x[13] - EKFdata.accel_mps2[0]) - 2.0 * x[1] * (x[14] -
		EKFdata.accel_mps2[1])) - 2.0 * x[0] * (x[15] - EKFdata.accel_mps2[2]);
	F[115] = (2.0 * x[0] * (x[14] -  EKFdata.accel_mps2[1]) + 4.0 * x[3] * (x[13] -
		 EKFdata.accel_mps2[0])) - 2.0 * x[1] * (x[15] -  EKFdata.accel_mps2[2]);

    F[125] = (2.0 * x[2]*x[2] + 2.0 * x[3]*x[3]) - 1.0;
    F[126] = 2.0 * x[0] * x[3] - 2.0 * x[1] * x[2];
    F[127] = -2.0 * x[0] * x[2] - 2.0 * x[1] * x[3];

    //Row 8
	F[128] = 2.0 * x[1] * (x[15] -  EKFdata.accel_mps2[2]) - 2.0 * x[3] * (x[13] -
		 EKFdata.accel_mps2[0]);
	F[129] = (4.0 * x[1] * (x[14] -  EKFdata.accel_mps2[1]) - 2.0 * x[2] * (x[13] -
		 EKFdata.accel_mps2[0])) + 2.0 * x[0] * (x[15] -  EKFdata.accel_mps2[2]);
	F[130] = -2.0 * x[1] * (x[13] -  EKFdata.accel_mps2[0]) - 2.0 * x[3] * (x[15] -
		 EKFdata.accel_mps2[2]);
	F[131] = (4.0 * x[3] * (x[14] -  EKFdata.accel_mps2[1]) - 2.0 * x[0] * (x[13] -
		 EKFdata.accel_mps2[0])) - 2.0 * x[2] * (x[15] -  EKFdata.accel_mps2[2]);

    F[141] = -2.0 * x[0] * x[3] - 2.0 * x[1] * x[2];
    F[142] = (2.0 * x[1]*x[1] + 2.0 * x[3]*x[3]) - 1.0;
    F[143] = 2.0 * x[0] * x[1] - 2.0 * x[2] * x[3];
	
    //Row 9
	F[144] = 2.0 * x[2] * (x[13] -  EKFdata.accel_mps2[0]) - 2.0 * x[1] * (x[14] -
		 EKFdata.accel_mps2[1]);
	F[145] = (4.0 * x[1] * (x[15] -  EKFdata.accel_mps2[2]) - 2.0 * x[3] * (x[13] -
		 EKFdata.accel_mps2[0])) - 2.0 * x[0] * (x[14] -  EKFdata.accel_mps2[1]);
	F[146] = (2.0 * x[0] * (x[13] -  EKFdata.accel_mps2[0]) - 2.0 * x[3] * (x[14] -
		 EKFdata.accel_mps2[1])) + 4.0 * x[2] * (x[15] -  EKFdata.accel_mps2[2]);
	F[147] = -2.0 * x[1] * (x[13] -  EKFdata.accel_mps2[0]) - 2.0 * x[2] * (x[14] -
		 EKFdata.accel_mps2[1]);

    F[157] = 2.0 * x[0] * x[2] - 2.0 * x[1] * x[3];
    F[158] = -2.0 * x[0] * x[1] - 2.0 * x[2] * x[3];
    F[159] = (2.0 * x[1]*x[1] + 2.0 * x[2]*x[2]) - 1.0;
}
