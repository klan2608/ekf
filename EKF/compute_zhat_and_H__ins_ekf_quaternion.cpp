#include "compute_xdot_and_F__ins_ekf_quaternion.h"
#include "compute_zhat_and_H__ins_ekf_quaternion.h"
#include "perform_ekf.h"
#include "matrixoperations.h"
#include "consts.h"

#include "macros.h"

void compute_zhat_and_H__ins_ekf_quaternion(FilterData &EKFdata, FLOAT_TYPE zhat[], FLOAT_TYPE H[])
{
	static FLOAT_TYPE M[9]; // Матрица перехода из географ. в собств.
	static FLOAT_TYPE C_mag2ned[9] = { 0.0, 0.0, 0.0,
									   0.0, 0.0, 0.0,
									   0.0, 0.0, 1.0}; // Матрица компенсации магнитного склонения
	static FLOAT_TYPE tempMatrix[9];

	static int i;


	//IdealField = {B * cos(MagneticDip), 0.0, B * sin(MagneticDip)};
	//#define MagneticDip 0
	//static const FLOAT_TYPE IdealField[3] = {1.0, 0.0, 0.0};

	//69.7
	//#define MagneticDip 1.2165
	static const FLOAT_TYPE IdealField[3] = {0.3469, 0.0, 0.9379};
	static const FLOAT_TYPE Hx = IdealField[0];
	static const FLOAT_TYPE Hy = IdealField[1];
	static const FLOAT_TYPE Hz = IdealField[2];

	static FLOAT_TYPE OutField[3];
	static FLOAT_TYPE* x;
	static FLOAT_TYPE cosMagDecl, sinMagDecl;


	x = EKFdata.xhat;

	M[0] = 1.0 - 2.0 * (x[2]*x[2] + x[3]*x[3]);
	M[1] = 2.0 * (x[1] * x[2] + x[3] * x[0]);
	M[2] = 2.0 * (x[1] * x[3] - x[2] * x[0]);
	M[3] = 2.0 * (x[1] * x[2] - x[3] * x[0]);
	M[4] = 1.0 - 2.0 * (x[1]*x[1] + x[3]*x[3]);
	M[5] = 2.0 * (x[2] * x[3] + x[1] * x[0]);
	M[6] = 2.0 * (x[1] * x[3] + x[2] * x[0]);
	M[7] = 2.0 * (x[2] * x[3] - x[1] * x[0]);
	M[8] = 1.0 - 2.0 * (x[1]*x[1] + x[2]*x[2]);

	C_mag2ned[0] =  cos(- EKFdata.MagDecl_deg * Deg2Rad);
	C_mag2ned[1] =  sin(- EKFdata.MagDecl_deg * Deg2Rad);
	C_mag2ned[3] = -sin(- EKFdata.MagDecl_deg * Deg2Rad);
	C_mag2ned[4] =  cos(- EKFdata.MagDecl_deg * Deg2Rad);

	/* mag3D_unitVector_in_body = C_ned2b*C_mag2ned*[1;0;0]; */
	MatrixOperations::multSquareMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*3*3>(M,C_mag2ned,tempMatrix,3);
	MatrixOperations::multMatrixes<FLOAT_TYPE,sizeof(FLOAT_TYPE)*3*1>(tempMatrix,const_cast<FLOAT_TYPE*>(IdealField),OutField,3,3,1);

	//3D magnetometer unit vector
	zhat[0] = OutField[0];
	zhat[1] = OutField[1];
	zhat[2] = OutField[2];
	// Pn; Pe; Alt
	zhat[3] = x[4];
	zhat[4] = x[5];
	zhat[5] = x[6];
	//Vn; Ve; Vd
	zhat[6] = x[7];
	zhat[7] = x[8];
	zhat[8] = x[9];

	/* H - 16x9 */
	/*
	H =

	[                                                                             2*Hy*q3*cos((pi*mag_declination_deg)/180) - 2*Hz*q2 + 2*Hx*q3*sin((pi*mag_declination_deg)/180),                                                                                       2*Hz*q3 + 2*Hy*q2*cos((pi*mag_declination_deg)/180) + 2*Hx*q2*sin((pi*mag_declination_deg)/180), Hy*(2*q1*cos((pi*mag_declination_deg)/180) + 4*q2*sin((pi*mag_declination_deg)/180)) - Hx*(4*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)) - 2*Hz*q0, 2*Hz*q1 - Hx*(4*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)) + Hy*(2*q0*cos((pi*mag_declination_deg)/180) + 4*q3*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                             2*Hz*q1 - 2*Hx*q3*cos((pi*mag_declination_deg)/180) + 2*Hy*q3*sin((pi*mag_declination_deg)/180), 2*Hz*q0 + Hx*(2*q2*cos((pi*mag_declination_deg)/180) - 4*q1*sin((pi*mag_declination_deg)/180)) - Hy*(4*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)),                                                                                       2*Hz*q3 + 2*Hx*q1*cos((pi*mag_declination_deg)/180) - 2*Hy*q1*sin((pi*mag_declination_deg)/180), 2*Hz*q2 - Hx*(2*q0*cos((pi*mag_declination_deg)/180) + 4*q3*sin((pi*mag_declination_deg)/180)) - Hy*(4*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[ Hx*(2*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)) - Hy*(2*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)), Hx*(2*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)) - 4*Hz*q1 - Hy*(2*q0*cos((pi*mag_declination_deg)/180) + 2*q3*sin((pi*mag_declination_deg)/180)), Hx*(2*q0*cos((pi*mag_declination_deg)/180) + 2*q3*sin((pi*mag_declination_deg)/180)) - 4*Hz*q2 + Hy*(2*q3*cos((pi*mag_declination_deg)/180) - 2*q0*sin((pi*mag_declination_deg)/180)),           Hx*(2*q1*cos((pi*mag_declination_deg)/180) + 2*q2*sin((pi*mag_declination_deg)/180)) + Hy*(2*q2*cos((pi*mag_declination_deg)/180) - 2*q1*sin((pi*mag_declination_deg)/180)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
	*/
	/*
	H =

	[                                                                             2*Hy*x[3]*cosMagDecl - 2*Hz*x[2] + 2*Hx*x[3]*sinMagDecl,                                                                                       2*Hz*x[3] + 2*Hy*x[2]*cosMagDecl + 2*Hx*x[2]*sinMagDecl, Hy*(2*x[1]*cosMagDecl + 4*x[2]*sinMagDecl) - Hx*(4*x[2]*cosMagDecl - 2*x[1]*sinMagDecl) - 2*Hz*x[0], 2*Hz*x[1] - Hx*(4*x[3]*cosMagDecl - 2*x[0]*sinMagDecl) + Hy*(2*x[0]*cosMagDecl + 4*x[3]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                             2*Hz*x[1] - 2*Hx*x[3]*cosMagDecl + 2*Hy*x[3]*sinMagDecl, 2*Hz*x[0] + Hx*(2*x[2]*cosMagDecl - 4*x[1]*sinMagDecl) - Hy*(4*x[1]*cosMagDecl + 2*x[2]*sinMagDecl),                                                                                       2*Hz*x[3] + 2*Hx*x[1]*cosMagDecl - 2*Hy*x[1]*sinMagDecl, 2*Hz*x[2] - Hx*(2*x[0]*cosMagDecl + 4*x[3]*sinMagDecl) - Hy*(4*x[3]*cosMagDecl - 2*x[0]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[ Hx*(2*x[2]*cosMagDecl - 2*x[1]*sinMagDecl) - Hy*(2*x[1]*cosMagDecl + 2*x[2]*sinMagDecl), Hx*(2*x[3]*cosMagDecl - 2*x[0]*sinMagDecl) - 4*Hz*x[1] - Hy*(2*x[0]*cosMagDecl + 2*x[3]*sinMagDecl), Hx*(2*x[0]*cosMagDecl + 2*x[3]*sinMagDecl) - 4*Hz*x[2] + Hy*(2*x[3]*cosMagDecl - 2*x[0]*sinMagDecl),           Hx*(2*x[1]*cosMagDecl + 2*x[2]*sinMagDecl) + Hy*(2*x[2]*cosMagDecl - 2*x[1]*sinMagDecl), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
	[                                                                                                                                                                           0,                                                                                                                                                                                     0,                                                                                                                                                                                     0,                                                                                                                                                                                     0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
	*/

	memset(H,0,sizeof(FLOAT_TYPE) * 16 * 9);

	sinMagDecl = sin( EKFdata.MagDecl_deg * Deg2Rad);
	cosMagDecl = cos( EKFdata.MagDecl_deg * Deg2Rad);

	H[0] = 2.0 *Hy*x[3]*cosMagDecl - 2.0 *Hz*x[2] + 2.0 *Hx*x[3]*sinMagDecl;
	H[1] = 2.0 *Hz*x[3] + 2.0 *Hy*x[2]*cosMagDecl + 2.0 *Hx*x[2]*sinMagDecl;
	H[2] = Hy*(2.0 *x[1]*cosMagDecl + 4.0 *x[2]*sinMagDecl) - Hx*(4.0 *x[2]*cosMagDecl - 2.0 *x[1]*sinMagDecl) - 2.0 *Hz*x[0];
	H[3] = 2.0 *Hz*x[1] - Hx*(4.0 *x[3]*cosMagDecl - 2.0 *x[0]*sinMagDecl) + Hy*(2.0 *x[0]*cosMagDecl + 4.0 *x[3]*sinMagDecl);

	H[16] = 2.0 *Hz*x[1] - 2.0 *Hx*x[3]*cosMagDecl + 2.0 *Hy*x[3]*sinMagDecl;
	H[17] = 2.0 *Hz*x[0] + Hx*(2.0 *x[2]*cosMagDecl - 4.0 *x[1]*sinMagDecl) - Hy*(4.0 *x[1]*cosMagDecl + 2.0 *x[2]*sinMagDecl);
	H[18] = 2.0 *Hz*x[3] + 2.0 *Hx*x[1]*cosMagDecl - 2.0 *Hy*x[1]*sinMagDecl;
	H[19] = 2*Hz*x[2] - Hx*(2*x[0]*cosMagDecl + 4*x[3]*sinMagDecl) - Hy*(4*x[3]*cosMagDecl - 2*x[0]*sinMagDecl);

	H[32] = Hx*(2.0 *x[2]*cosMagDecl - 2.0 *x[1]*sinMagDecl) - Hy*(2.0 *x[1]*cosMagDecl + 2.0 *x[2]*sinMagDecl);
	H[33] = Hx*(2.0 *x[3]*cosMagDecl - 2.0 *x[0]*sinMagDecl) - 4.0 *Hz*x[1] - Hy*(2.0 *x[0]*cosMagDecl + 2.0 *x[3]*sinMagDecl);
	H[34] = Hx*(2.0 *x[0]*cosMagDecl + 2.0 *x[3]*sinMagDecl) - 4.0 *Hz*x[2] + Hy*(2.0 *x[3]*cosMagDecl - 2.0 *x[0]*sinMagDecl);
	H[35] = Hx*(2.0 *x[1]*cosMagDecl + 2.0 *x[2]*sinMagDecl) + Hy*(2.0 *x[2]*cosMagDecl - 2.0 *x[1]*sinMagDecl);

	for ( i = 3; i < 9; i++)
		H[ 16*i + 4 + (i - 3) ] = 1.0;
}
