#ifndef __COMPUTE_XDOT_AND_F__INS_EKF_QUATERNION_H__
#define __COMPUTE_XDOT_AND_F__INS_EKF_QUATERNION_H__
/* Include files */
#include <cmath>
#include <stddef.h>
//#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include"ekfTypes.h"

#include "macros.h"

/* Function Declarations */
extern void compute_xdot_and_F__ins_ekf_quaternion(FilterData& EKFdata, FLOAT_TYPE xdot[16], FLOAT_TYPE F[256]);
#endif
