#include "ekfTypes.h"

FilterData::FilterData() {
	memset(this,0,sizeof(FilterData));
}

void FilterData::SetInitX(FLOAT_TYPE InitQuat[4],FLOAT_TYPE InitPosition[3],FLOAT_TYPE InitVelocity[3])
{
	for (i = 0; i < 4; i++)
		xhat[i] = InitQuat[i];
	for (i = 0; i < 3; i++)
		xhat[i+4] = InitPosition[i];
	for (i = 0; i < 3; i++)
		xhat[i+7] = InitVelocity[i];
}

void FilterData::SetInitBias(FLOAT_TYPE InitBiasGyro[3], FLOAT_TYPE InitBiasAccel[3])
{
	for (i = 0; i < 3; i++)
		xhat[i+10] = InitBiasGyro[i];
	for (i = 0; i < 3; i++)
		xhat[i+13] = InitBiasAccel[i];
}

void FilterData::FillInitSigmas()
{
	sigmas.mag3D_unitVector_meas = 0.2;
	sigmas.pos_meas_m            = 10.0;
	sigmas.vel_meas_mps          = 1.0;
	sigmas.gyro_bias_rps         = 0.06;
	sigmas.accel_bias_mps2       =  0.2;
	sigmas.quat_process_noise = 0.02;         // Quaternion process noise
	sigmas.pos_process_noise_m = 7.0;           // Position process noise, m
	sigmas.vel_process_noise_mps = 2.0;         // Velocity process noise, m/s
	sigmas.gyroBias_process_noise_rps = 1e-3; // Gyro bias process noise, rad/s
	sigmas.accelBias_process_noise_mps2 = 1e-3; // Accel bias process noise, m/s^2
	sigmas.large_quat_uncertainty = 0.1;
	sigmas.large_pos_uncertainty_m = 150.0;
	sigmas.large_vel_uncertainty_mps = 10.0;
}

void FilterData::SetNewAccelData(FLOAT_TYPE Ax, FLOAT_TYPE Ay, FLOAT_TYPE Az)
{
	accel_mps2[0] = Ax;
	accel_mps2[1] = Ay;
	accel_mps2[2] = Az;
}

void FilterData::SetNewGyroData(FLOAT_TYPE Gx, FLOAT_TYPE Gy, FLOAT_TYPE Gz)
{
	gyro_rps[0] = Gx;
	gyro_rps[1] = Gy;
	gyro_rps[2] = Gz;
}

void FilterData::FillZMag(FLOAT_TYPE Mx, FLOAT_TYPE My, FLOAT_TYPE Mz)
{
	z[0] = Mx;
	z[1] = My;
	z[2] = Mz;
}

void FilterData::FillZPos(FLOAT_TYPE Nm,FLOAT_TYPE Em,FLOAT_TYPE Hm)
{
	z[3] = Nm;
	z[4] = Em;
	z[5] = Hm;
}

void FilterData::FillZVelocity(FLOAT_TYPE VN,FLOAT_TYPE VE,FLOAT_TYPE VH)
{
	z[6] = VN;
	z[7] = VE;
	z[8] = VH;
}

void FilterData::EmptyZR()
{
	memset(z,0,sizeof(z));
	memset(R,0,sizeof(R));
}

void FilterData::FillR()
{
	// R: sampled measurement uncertainty, R(k)
	for ( i = 0; i < 3; i++ )
		R[i + 9*i] =  sigmas.mag3D_unitVector_meas* sigmas.mag3D_unitVector_meas; // Mag. unit vector noise

	for ( i = 3; i < 6; i++ )
		R[i + 9*i] =  sigmas.pos_meas_m* sigmas.pos_meas_m;  // North-East-Alt position measurement noise, m

	for ( i = 6; i < 9; i++ )
		R[i + 9*i] =  sigmas.vel_meas_mps* sigmas.vel_meas_mps; // NED velocity measurement noise, m/s

	// Make sure no elements of R diagonal are zero! Arbitrarily use a
	// minimum of (1e-3)^2.
	for ( i = 0; i < 9; i++ )
	{
		if (  R[i + 9*i] < MATRIX_EPS )
			R[i + 9*i] = MATRIX_EPS;
	}
}

void FilterData::FillPQ()
{
	//Fill P
	// Start with "large" uncertainties and build initial covariance (state uncertainty)
	for ( i = 0; i < 4; i++ ) {
		P[i + 16*i] =  sigmas.large_quat_uncertainty* sigmas.large_quat_uncertainty; // init quaternion (NED-to-body) uncertainty
		Q[i + 16*i] =  sigmas.quat_process_noise* sigmas.quat_process_noise; // quaternion process noise
	}

	for ( i = 4; i < 7; i++ ) {
		P[i + 16*i] =  sigmas.large_pos_uncertainty_m* sigmas.large_pos_uncertainty_m; // init North-East-Alt position uncertainties, m
		Q[i + 16*i] =  sigmas.pos_process_noise_m* sigmas.pos_process_noise_m;  // North-East-Alt position process noise, m
	}

	for ( i = 7; i < 10; i++ ) {
		P[i + 16*i] =  sigmas.large_vel_uncertainty_mps* sigmas.large_vel_uncertainty_mps; // init NED velocity uncertainties, m/s
		Q[i + 16*i] =  sigmas.vel_process_noise_mps* sigmas.vel_process_noise_mps; // NED velocity process noise, m/s
	}

	for ( i = 10; i < 13; i++ ) {
		P[i + 16*i] =  sigmas.gyro_bias_rps* sigmas.gyro_bias_rps; // init XYZ gyro bias uncertainties, rad/s
		Q[i + 16*i] =  sigmas.gyroBias_process_noise_rps* sigmas.gyroBias_process_noise_rps; // XYZ gyro bias process noise, rad/s
	}

	for ( i = 13; i < 16; i++ ) {
		P[i + 16*i] =  sigmas.accel_bias_mps2* sigmas.accel_bias_mps2; // init XYZ accel bias uncertainties, m/s^2
		Q[i + 16*i] =  sigmas.accelBias_process_noise_mps2* sigmas.accelBias_process_noise_mps2; // XYZ accel bias process noise, m/s^2
	}

	/*
	 Make sure no elements of P diagonal are zero! Arbitrarily use a
	 minimum of (1e-3)^2.  (For example, if gyroBias was set to zero when
	 generating the sensor measurement, we still want some initial P.)
	 */
	for ( i = 0; i < 16; i++ )
	{
		if (  Q[i + 16*i]< MATRIX_EPS )
			Q[i + 16*i] = MATRIX_EPS;

		if (  P[i + 16*i]< MATRIX_EPS )
			P[i + 16*i] = MATRIX_EPS;
	}
}
